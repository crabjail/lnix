# Contributing

**Table Of Contents**

- [Building](#building)
- [Coding style](#coding-style)
  - [Python](#python)
  - [Rust](#rust)

## Building

While we use meson to build crablock for production. However, the recommended
way to build crablock during development is to use cargo directly.

## Coding style

## Python

Run [black](https://pypi.org/project/black/) to format your code,
[isort](https://pypi.org/project/isort/) to format your imports
and pylint to catch some common mistakes before committing.

Also read [PEP8](https://www.python.org/dev/peps/pep-0008/),
you will be a better programmer.

## Rust

Run `cargo fmt` to format your code before committing. Keep in mind that this
will not format macros, you have to format them yourself.

Also run `cargo clippy` and fix all of the warnings/errors related to your code
or allow them if you believe that they are false positives. `cargo clippy`
should run without warnings/errors except for warnings/errors caused by
newer/older versions of clippy. Moreover you should look if
`cargo clippy -- --warn clippy::pedantic --warn clippy::nursery` reports
warnings/errors which seem to be correct.

### Ordering of Attributes

```
#[cfg(_)]
#[allow(_)]
#[derive(Debug, Default, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[must_use]
#[non_exhaustive]
#[repr(_)]
```

### Doc Comments

Doc comments on functions should start with one short sentence completing a
sentence starting with 'This function ...' like 'This function adds two.'

### TODO: UNSAFE GUIDELINE

### TODO: FFI GUIDELINE
