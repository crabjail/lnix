// SPDX-License-Identifier: Apache-2.0 OR MIT

/*
 * Copyright © 2023 The lnix Authors
 *
 * Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
 * https://www.apache.org/licenses/LICENSE-2.0> or the MIT license
 * <LICENSE-MIT or https://opensource.org/licenses/MIT>, at your
 * option. This file may not be copied, modified, or distributed
 * except according to those terms.
 */

//! Uid/Gid types.

use std::num::ParseIntError;
use std::str::FromStr;

use linux_raw_sys::general::{__kernel_gid_t, __kernel_uid_t};

/// Raw integer uid (`uid_t`).
#[doc(alias = "uid_t")]
pub type RawUid = __kernel_uid_t;

/// Raw integer gid (`gid_t`).
#[doc(alias = "gid_t")]
pub type RawGid = __kernel_gid_t;

/// A uid.
#[derive(Debug, Default, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[repr(transparent)]
pub struct Uid(pub(crate) RawUid);
impl Uid {
    /// Converts a `Uid` to a `RawUid`.
    pub fn as_raw(self) -> RawUid {
        self.0
    }

    /// Converts a `RawUid` to a `Uid`.
    ///
    /// # Panics
    ///
    /// if `raw_uid` is `-1`.
    pub fn from_raw(raw_uid: RawUid) -> Self {
        assert!(raw_uid != -1_i32 as u32);
        Self(raw_uid)
    }
}
impl FromStr for Uid {
    type Err = ParseIntError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(Self(s.parse()?))
    }
}

/// A gid.
#[derive(Debug, Default, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[repr(transparent)]
pub struct Gid(pub(crate) RawGid);
impl Gid {
    /// Converts a `Gid` to a `RawGid`.
    pub fn as_raw(self) -> RawGid {
        self.0
    }

    /// Converts a `RawGid` to a `Gid`.
    ///
    /// # Panics
    ///
    /// if `raw_gid` is `-1`.
    pub fn from_raw(raw_gid: RawGid) -> Self {
        assert!(raw_gid != -1_i32 as u32);
        Self(raw_gid)
    }
}
impl FromStr for Gid {
    type Err = ParseIntError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(Self(s.parse()?))
    }
}
