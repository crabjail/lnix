// SPDX-License-Identifier: Apache-2.0 OR MIT

/*
 * Copyright © 2022 The lnix Authors
 *
 * Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
 * https://www.apache.org/licenses/LICENSE-2.0> or the MIT license
 * <LICENSE-MIT or https://opensource.org/licenses/MIT>, at your
 * option. This file may not be copied, modified, or distributed
 * except according to those terms.
 */

//! `ioctl` operations for namespaces.

use core::fmt;
use rustix::io::Errno as RustixErrno;
use rustix::ioctl::{Ioctl, IoctlOutput, Opcode, ioctl};
use std::ffi::c_void;
use std::os::unix::prelude::*;
use std::ptr;
use std::ptr::addr_of_mut;
use std::str::FromStr;

use crate::ugid::{RawUid, Uid};

const NSIO: u8 = 0xb7;
const NS_GET_USERNS: Opcode = Opcode::none::<()>(NSIO, 0x1);
const NS_GET_PARENT: Opcode = Opcode::none::<()>(NSIO, 0x2);
const NS_GET_NSTYPE: Opcode = Opcode::none::<()>(NSIO, 0x3);
const NS_GET_OWNER_UID: Opcode = Opcode::none::<()>(NSIO, 0x4);

#[derive(Debug, Default)]
struct NsGetUserns;
unsafe impl Ioctl for NsGetUserns {
    type Output = OwnedFd;

    const OPCODE: Opcode = NS_GET_USERNS;
    const IS_MUTATING: bool = true;

    fn as_ptr(&mut self) -> *mut c_void {
        ptr::null_mut()
    }

    unsafe fn output_from_ptr(
        raw_fd: IoctlOutput,
        _: *mut c_void,
    ) -> Result<Self::Output, RustixErrno> {
        Ok(unsafe { OwnedFd::from_raw_fd(raw_fd) })
    }
}

#[derive(Debug, Default)]
struct NsGetParent;
unsafe impl Ioctl for NsGetParent {
    type Output = OwnedFd;

    const OPCODE: Opcode = NS_GET_PARENT;
    const IS_MUTATING: bool = true;

    fn as_ptr(&mut self) -> *mut c_void {
        ptr::null_mut()
    }

    unsafe fn output_from_ptr(
        raw_fd: IoctlOutput,
        _: *mut c_void,
    ) -> Result<Self::Output, RustixErrno> {
        Ok(unsafe { OwnedFd::from_raw_fd(raw_fd) })
    }
}

#[derive(Debug, Default)]
struct NsGetNstype;
unsafe impl Ioctl for NsGetNstype {
    type Output = NsType;

    const OPCODE: Opcode = NS_GET_NSTYPE;
    const IS_MUTATING: bool = true;

    fn as_ptr(&mut self) -> *mut c_void {
        ptr::null_mut()
    }

    unsafe fn output_from_ptr(
        raw_ns_type: IoctlOutput,
        _: *mut c_void,
    ) -> Result<Self::Output, RustixErrno> {
        match raw_ns_type {
            t if t == NsType::Mnt as _ => Ok(NsType::Mnt),
            t if t == NsType::Cgroup as _ => Ok(NsType::Cgroup),
            t if t == NsType::Uts as _ => Ok(NsType::Uts),
            t if t == NsType::Ipc as _ => Ok(NsType::Ipc),
            t if t == NsType::User as _ => Ok(NsType::User),
            t if t == NsType::Pid as _ => Ok(NsType::Pid),
            t if t == NsType::Net as _ => Ok(NsType::Net),
            t if t == NsType::Time as _ => Ok(NsType::Time),
            _ => unreachable!("Unexpected ns_type"),
        }
    }
}

#[derive(Debug, Default)]
struct NsGetOwnerUid {
    uid: RawUid,
}
unsafe impl Ioctl for NsGetOwnerUid {
    type Output = Uid;

    const OPCODE: Opcode = NS_GET_OWNER_UID;
    const IS_MUTATING: bool = true;

    fn as_ptr(&mut self) -> *mut c_void {
        addr_of_mut!(self.uid).cast()
    }

    unsafe fn output_from_ptr(
        _: IoctlOutput,
        uid: *mut c_void,
    ) -> Result<Self::Output, RustixErrno> {
        Ok(Uid::from_raw(unsafe { *uid.cast() }))
    }
}

/// `ioctl(ns_fd, NS_GET_USERNS)`
///
/// # Safety
///
/// `fd` must refer to a `/proc/pid/ns/*` file.
pub unsafe fn ioctl_ns_get_userns<FD: AsFd>(fd: FD) -> Result<OwnedFd, RustixErrno> {
    unsafe { ioctl(fd, NsGetUserns) }
}

/// `ioctl(ns_fd, NS_GET_PARENT)`
///
/// # Safety
///
/// `fd` must refer to a `/proc/pid/ns/*` file.
pub unsafe fn ioctl_ns_get_parent<FD: AsFd>(fd: FD) -> Result<OwnedFd, RustixErrno> {
    unsafe { ioctl(fd, NsGetParent) }
}

/// `ioctl(ns_fd, NS_GET_NSTYPE)`
///
/// # Safety
///
/// `fd` must refer to a `/proc/pid/ns/*` file.
pub unsafe fn ioctl_ns_get_nstype<FD: AsFd>(fd: FD) -> Result<NsType, RustixErrno> {
    unsafe { ioctl(fd, NsGetNstype) }
}

/// `ioctl(ns_fd, NS_GET_OWNER_UID)`
///
/// # Safety
///
/// `fd` must refer to a `/proc/pid/ns/*` file.
pub unsafe fn ioctl_ns_get_owner_uid<FD: AsFd>(fd: FD) -> Result<Uid, RustixErrno> {
    unsafe { ioctl(fd, NsGetOwnerUid::default()) }
}

/// Known namespace types.
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[non_exhaustive]
pub enum NsType {
    /// `cgroup_namespaces(7)`
    Cgroup = 0x02000000,
    /// `ipc_namespaces(7)`
    Ipc = 0x08000000,
    /// `network_namespaces(7)`
    Net = 0x40000000,
    /// `mount_namespaces(7)`
    Mnt = 0x00020000,
    /// `pid_namespaces(7)`
    Pid = 0x20000000,
    /// `time_namespaces(7)`
    Time = 0x00000080,
    /// `user_namespaces(7)`
    User = 0x10000000,
    /// `uts_namespaces(7)`
    Uts = 0x04000000,
}
impl fmt::Display for NsType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str(match self {
            Self::Cgroup => "cgroup",
            Self::Ipc => "ipc",
            Self::Net => "net",
            Self::Mnt => "mnt",
            Self::Pid => "pid",
            Self::Time => "time",
            Self::User => "user",
            Self::Uts => "uts",
        })
    }
}
impl FromStr for NsType {
    type Err = ParseNsTypeError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "cgroup" => Ok(Self::Cgroup),
            "ipc" => Ok(Self::Ipc),
            "net" => Ok(Self::Net),
            "mnt" => Ok(Self::Mnt),
            "pid" => Ok(Self::Pid),
            "time" => Ok(Self::Time),
            "user" => Ok(Self::User),
            "uts" => Ok(Self::Uts),
            _ => Err(ParseNsTypeError),
        }
    }
}

/// An error which can be returned when parsing a namespace type.
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct ParseNsTypeError;
impl fmt::Display for ParseNsTypeError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        "provided string was not a valid namespace type".fmt(f)
    }
}
impl std::error::Error for ParseNsTypeError {}
