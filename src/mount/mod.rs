// SPDX-License-Identifier: Apache-2.0 OR MIT

/*
 * Copyright © 2022 The lnix Authors
 *
 * Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
 * https://www.apache.org/licenses/LICENSE-2.0> or the MIT license
 * <LICENSE-MIT or https://opensource.org/licenses/MIT>, at your
 * option. This file may not be copied, modified, or distributed
 * except according to those terms.
 */

//! Mount operations.
//!
//! # Supported syscalls
//!
//! - `fsopen`
//!   - `FilesystemContext::fsopen`
//! - `fspick`
//!   - `FilesystemContext::fspick`
//! - `fsconfig`
//!   - `FilesystemContext::fsconfig_set_*`
//!   - `FilesystemContext::fsconfig_cmd_*`
//! - `fsmount`
//!   - `FilesystemContext::fsmount`
//! - `open_tree`
//!   - `MountObject::open_tree`
//! - `move_mount`
//!   - `MountObject::move_to`
//!   - `move_mount`
//! - `mount_setattr`
//! - `umount2`
//! - `pivot_root`

pub use rustix::mount::{UnmountFlags as UmountFlags, unmount as umount2};

mod pivot_root;
#[allow(deprecated)]
pub use pivot_root::pivot_root;

pub mod mountfd;
