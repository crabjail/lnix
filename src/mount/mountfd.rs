// SPDX-License-Identifier: Apache-2.0 OR MIT

/*
 * Copyright © 2023 The lnix Authors
 *
 * Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
 * https://www.apache.org/licenses/LICENSE-2.0> or the MIT license
 * <LICENSE-MIT or https://opensource.org/licenses/MIT>, at your
 * option. This file may not be copied, modified, or distributed
 * except according to those terms.
 */

//! The new fd based linux mount API.
//!
//! # Examples
//!
//! - `mount -t tmpfs -o size=10m,ro,nosuid,nodev tmpfs /tmp`:
//!
//! ```rust,no_run
//! # use lnix::mount::mountfd::*;
//! # use rustix::fs::CWD;
//! let mut fs_fd = FilesystemContext::fsopen("tmpfs", FsOpenFlags::default())?;
//! fs_fd.fsconfig_set_string("size", "10m")?;
//! fs_fd.fsconfig_set_flag("ro")?;
//! fs_fd.fsconfig_cmd_create()?;
//! let mut mfd = fs_fd.fsmount(
//!     FsMountFlags::default(),
//!     MountAttrFlags::RDONLY
//!         | MountAttrFlags::NOSUID
//!         | MountAttrFlags::NODEV,
//! )?;
//! mfd.move_to(CWD, "/tmp", MoveMountFlags::default())?;
//! # Ok::<(), Box<dyn std::error::Error>>(())
//! ```
//!
//! - `mount --rbind -o ro /mnt/src /mnt/dst`:
//!
//! ```rust,no_run
//! # use lnix::mount::mountfd::*;
//! # use rustix::fs::CWD;
//! # use std::os::fd::AsFd;
//! let mut mfd = MountFd::open_tree(
//!     CWD,
//!     "/mnt/src",
//!     OpenTreeFlags::CLONE | OpenTreeFlags::RECURSIVE,
//! )?;
//! mfd.setattr(
//!     MountSetattrFlags::RECURSIVE,
//!     MountAttr::new().set_flags(MountAttrFlags::RDONLY),
//! )?;
//! mfd.move_to(CWD, "/mnt/dst", MoveMountFlags::default())?;
//! # Ok::<(), Box<dyn std::error::Error>>(())
//! ```

use std::ffi::{CString, OsStr};
use std::os::unix::prelude::*;
use std::path::Path;
use std::ptr;

use bitflags::bitflags;

use crate::error::{Errno, cvt};
use crate::std_suppl::prelude::*;

bitflags! {
    /// Flags for `fsopen`.
    #[derive(Debug, Default, Clone, Copy, PartialEq, Eq, Hash)]
    pub struct FsOpenFlags: u32 {
        /// Set the `CLOEXEC` flag.
        ///
        /// **Note**: [`fsopen`](FilesystemContext::fsopen) always sets this flag.
        const CLOEXEC = sys::FSOPEN_CLOEXEC;
    }

    /// Flags for `fspick`.
    #[derive(Debug, Default, Clone, Copy, PartialEq, Eq, Hash)]
    pub struct FsPickFlags: u32 {
        /// Set the `CLOEXEC` flag.
        ///
        /// **Note**: [`fspick`](FilesystemContext::fspick) always sets this flag.
        const CLOEXEC = sys::FSPICK_CLOEXEC;
        /// FSPICK_SYMLINK_NOFOLLOW
        const SYMLINK_NOFOLLOW = sys::FSPICK_SYMLINK_NOFOLLOW;
        /// FSPICK_NO_AUTOMOUNT
        const NO_AUTOMOUNT = sys::FSPICK_NO_AUTOMOUNT;
        /// FSPICK_EMPTY_PATH
        const EMPTY_PATH = sys::FSPICK_EMPTY_PATH;
    }

    /// Flags for `fsmount`.
    #[derive(Debug, Default, Clone, Copy, PartialEq, Eq, Hash)]
    pub struct FsMountFlags: u32 {
        /// Set the `CLOEXEC` flag.
        ///
        /// **Note**: [`fsmount`](FilesystemContext::fsmount) always sets this flag.
        const CLOEXEC = sys::FSMOUNT_CLOEXEC;
    }

    /// `MountAttrFlags`
    #[derive(Debug, Default, Clone, Copy, PartialEq, Eq, Hash)]
    pub struct MountAttrFlags: u32 {
        /// Mount read-only.
        const RDONLY = sys::MOUNT_ATTR_RDONLY;
        /// Ignore suid and sgid bits.
        const NOSUID = sys::MOUNT_ATTR_NOSUID;
        /// Ignore character / block device files.
        const NODEV = sys::MOUNT_ATTR_NODEV;
        /// Forbid execution of files.
        const NOEXEC = sys::MOUNT_ATTR_NOEXEC;
        /// Mask for `MOUNT_ATTR_RELATIME|MOUNT_ATTR_NOATIME|MOUNT_ATTR_STRICTATIME`.
        const _ATIME = sys::MOUNT_ATTR__ATIME;
        /// `relatime` (default).
        const RELATIME = sys::MOUNT_ATTR_RELATIME;
        /// `noatime`.
        const NOATIME = sys::MOUNT_ATTR_NOATIME;
        /// `strictatime`.
        const STRICTATIME = sys::MOUNT_ATTR_STRICTATIME;
        /// `noatime` only for directories.
        const NODIRATIME = sys::MOUNT_ATTR_NODIRATIME;
        /// Do not follow symlinks.
        const NOSYMFOLLOW = sys::MOUNT_ATTR_NOSYMFOLLOW;
    }

    /// Flags for `open_tree`.
    #[derive(Debug, Default, Clone, Copy, PartialEq, Eq, Hash)]
    pub struct OpenTreeFlags: u32 {
        /// AT_EMPTY_PATH
        const EMPTY_PATH = libc::AT_EMPTY_PATH as u32;
        /// AT_NO_AUTOMOUNT
        const NO_AUTOMOUNT = libc::AT_NO_AUTOMOUNT as u32;
        /// AT_SYMLINK_NOFOLLOW
        const SYMLINK_NOFOLLOW = libc::AT_SYMLINK_NOFOLLOW as u32;
        /// AT_RECURSIVE
        const RECURSIVE = libc::AT_RECURSIVE as u32;
        /// Set the `CLOEXEC` flag.
        ///
        /// **Note**: [`open_tree`](MountFd::open_tree) always sets this flag.
        const CLOEXEC = sys::OPEN_TREE_CLOEXEC;
        /// OPEN_TREE_CLONE
        const CLONE = sys::OPEN_TREE_CLONE;
    }

    /// Flags for `move_mount` and `MountFd::move_to`.
    #[derive(Debug, Default, Clone, Copy, PartialEq, Eq, Hash)]
    pub struct MoveMountFlags: u32 {
        /// Follow symlinks in `from_pathname`.
        const F_SYMLINKS = sys::MOVE_MOUNT_F_SYMLINKS;
        /// Follow automounts on `from_pathname`.
        const F_AUTOMOUNTS = sys::MOVE_MOUNT_F_AUTOMOUNTS;
        /// AT_EMPTY_PATH for `from_pathname`.
        const F_EMPTY_PATH = sys::MOVE_MOUNT_F_EMPTY_PATH;
        /// Follow symlinks inn `to_pathname`.
        const T_SYMLINKS = sys::MOVE_MOUNT_T_SYMLINKS;
        /// Follow automounts on `to_pathname`.
        const T_AUTOMOUNTS = sys::MOVE_MOUNT_T_AUTOMOUNTS;
        /// AT_EMPTY_PATH for `to_pathname`.
        const T_EMPTY_PATH = sys::MOVE_MOUNT_T_EMPTY_PATH;
        /// MOVE_MOUNT_SET_GROUP
        const SET_GROUP = sys::MOVE_MOUNT_SET_GROUP;
        /// MOVE_MOUNT_BENEATH
        const BENEATH = sys::MOVE_MOUNT_BENEATH;
    }

    /// Flags for `mount_setattr`.
    #[derive(Debug, Default, Clone, Copy, PartialEq, Eq, Hash)]
    pub struct MountSetattrFlags: u32 {
        /// AT_EMPTY_PATH
        const EMPTY_PATH = libc::AT_EMPTY_PATH as u32;
        /// Change the mount properties recursive.
        const RECURSIVE = libc::AT_RECURSIVE as u32;
        /// Do not follow trailing symlinks.
        const SYMLINK_NOFOLLOW = libc::AT_SYMLINK_NOFOLLOW as u32;
        /// Don't follow automounts.
        const NO_AUTOMOUNT = libc::AT_NO_AUTOMOUNT as u32;
    }
}
impl FsOpenFlags {
    fn as_rustix(self) -> rustix::mount::FsOpenFlags {
        rustix::mount::FsOpenFlags::from_bits_retain(self.bits())
    }
}
impl FsPickFlags {
    fn as_rustix(self) -> rustix::mount::FsPickFlags {
        rustix::mount::FsPickFlags::from_bits_retain(self.bits())
    }
}
impl FsMountFlags {
    fn as_rustix(self) -> rustix::mount::FsMountFlags {
        rustix::mount::FsMountFlags::from_bits_retain(self.bits())
    }
}
impl MountAttrFlags {
    fn as_rustix(self) -> rustix::mount::MountAttrFlags {
        rustix::mount::MountAttrFlags::from_bits_retain(self.bits())
    }
}
impl OpenTreeFlags {
    fn as_rustix(self) -> rustix::mount::OpenTreeFlags {
        rustix::mount::OpenTreeFlags::from_bits_retain(self.bits())
    }
}
impl MoveMountFlags {
    fn as_rustix(self) -> rustix::mount::MoveMountFlags {
        rustix::mount::MoveMountFlags::from_bits_retain(self.bits())
    }
}

/* TODO
enum Message {
    Error(OsString),
    Info(OsString),
    Warning(OsString),
}
*/

/// `FilesystemContext`
#[derive(Debug)]
pub struct FilesystemContext(OwnedFd);
impl FilesystemContext {
    /// Creates a new `FilesystemContext` for `fs_name`.
    ///
    /// # Examples
    ///
    /// ```rust,no_run
    /// # use lnix::mount::mountfd::*;
    /// let mut fs_fd = FilesystemContext::fsopen("tmpfs", FsOpenFlags::default())?;
    /// # Ok::<(), Box<dyn std::error::Error>>(())
    /// ```
    pub fn fsopen(fs_name: &str, mut flags: FsOpenFlags) -> Result<Self, Errno> {
        flags |= FsOpenFlags::CLOEXEC;
        let fs_fd = rustix::mount::fsopen(fs_name, flags.as_rustix())?;
        Ok(Self(fs_fd))
    }

    /// Creates a new `FilesystemContext` for an existing superblock.
    pub fn fspick<Fd: AsFd, P: AsRef<Path>>(
        dfd: Fd,
        path: P,
        mut flags: FsPickFlags,
    ) -> Result<Self, Errno> {
        flags |= FsPickFlags::CLOEXEC;
        let fs_fd = rustix::mount::fspick(dfd.as_fd(), path.as_ref(), flags.as_rustix())?;
        Ok(Self(fs_fd))
    }

    /// Sets `key` to `true`.
    pub fn fsconfig_set_flag(&self, key: &str) -> Result<&Self, Errno> {
        rustix::mount::fsconfig_set_flag(self.as_fd(), key)?;

        Ok(self)
    }

    /// Sets `key` to `value`.
    pub fn fsconfig_set_string<V: AsRef<OsStr>>(
        &self,
        key: &str,
        value: V,
    ) -> Result<&Self, Errno> {
        rustix::mount::fsconfig_set_string(self.as_fd(), key, value.as_ref())?;

        Ok(self)
    }

    /// Sets `key` to `data`.
    pub fn fsconfig_set_binary(&self, key: &str, data: &[u8]) -> Result<&Self, Errno> {
        rustix::mount::fsconfig_set_binary(self.as_fd(), key, data)?;

        Ok(self)
    }

    /// Sets `key` to  `path`.
    pub fn fsconfig_set_path<P: AsRef<Path>, Fd: AsFd>(
        &self,
        key: &str,
        path: P,
        fd: Fd,
    ) -> Result<&Self, Errno> {
        rustix::mount::fsconfig_set_path(self.as_fd(), key, path.as_ref(), fd.as_fd())?;

        Ok(self)
    }

    /// Sets `key` to  `fd`.
    pub fn fsconfig_set_path_empty<Fd: AsFd>(&self, key: &str, fd: Fd) -> Result<&Self, Errno> {
        rustix::mount::fsconfig_set_path_empty(self.as_fd(), key, fd.as_fd())?;

        Ok(self)
    }

    /// Sets `key` to `fd`.
    pub fn fsconfig_set_fd(&self, key: &str, fd: BorrowedFd<'_>) -> Result<&Self, Errno> {
        rustix::mount::fsconfig_set_fd(self.as_fd(), key, fd)?;

        Ok(self)
    }

    /// Creates a new superblock or reuses a existing superblock.
    pub fn fsconfig_cmd_create(&self) -> Result<&Self, Errno> {
        rustix::mount::fsconfig_create(self.as_fd())?;

        Ok(self)
    }

    /// Creates a new superblock, fails if an existing one would be reused.
    pub fn fsconfig_cmd_create_excl(&self) -> Result<&Self, Errno> {
        cvt(unsafe {
            sys::fsconfig(
                self.as_raw_fd(),
                sys::FSCONFIG_CMD_CREATE_EXCL,
                ptr::null(),
                ptr::null(),
                0,
            )
        })?;

        Ok(self)
    }

    /// Reconfigures a superblock.
    pub fn fsconfig_cmd_reconfigure(&self) -> Result<&Self, Errno> {
        rustix::mount::fsconfig_reconfigure(self.as_fd())?;

        Ok(self)
    }

    /// Creates a mount object from this `FilesystemContext`.
    pub fn fsmount(
        &self,
        mut flags: FsMountFlags,
        attr_flags: MountAttrFlags,
    ) -> Result<MountFd, Errno> {
        flags |= FsMountFlags::CLOEXEC;
        let mfd = rustix::mount::fsmount(self.as_fd(), flags.as_rustix(), attr_flags.as_rustix())?;
        Ok(MountFd(mfd))
    }
}
impl AsFd for FilesystemContext {
    fn as_fd(&self) -> BorrowedFd<'_> {
        self.0.as_fd()
    }
}
impl AsRawFd for FilesystemContext {
    fn as_raw_fd(&self) -> RawFd {
        self.0.as_raw_fd()
    }
}
impl From<FilesystemContext> for OwnedFd {
    fn from(context: FilesystemContext) -> Self {
        context.0
    }
}

/// `MountFd`
#[derive(Debug)]
pub struct MountFd(OwnedFd);
impl MountFd {
    /// Picks or Clones the mount object at `filename`.
    pub fn open_tree<Fd: AsFd, P: AsRef<Path>>(
        dfd: Fd,
        filename: P,
        mut flags: OpenTreeFlags,
    ) -> Result<Self, Errno> {
        flags |= OpenTreeFlags::CLOEXEC;
        Ok(Self(rustix::mount::open_tree(
            dfd.as_fd(),
            filename.as_ref(),
            flags.as_rustix(),
        )?))
    }

    /// Changes the mount properties.
    pub fn setattr(
        &self,
        mut flags: MountSetattrFlags,
        attr: &mut MountAttr,
    ) -> Result<&Self, Errno> {
        flags |= MountSetattrFlags::EMPTY_PATH;
        mount_setattr(self, "", flags, attr)?;

        Ok(self)
    }

    /// Attaches or Moves this mount object to `pathname`.
    pub fn move_to<Fd: AsFd, P: AsRef<Path>>(
        &self,
        dfd: Fd,
        pathname: P,
        mut flags: MoveMountFlags,
    ) -> Result<(), Errno> {
        flags |= MoveMountFlags::F_EMPTY_PATH;
        rustix::mount::move_mount(
            self.as_fd(),
            "",
            dfd.as_fd(),
            pathname.as_ref(),
            flags.as_rustix(),
        )?;

        Ok(())
    }
}
impl AsFd for MountFd {
    fn as_fd(&self) -> BorrowedFd<'_> {
        self.0.as_fd()
    }
}
impl AsRawFd for MountFd {
    fn as_raw_fd(&self) -> RawFd {
        self.0.as_raw_fd()
    }
}
impl From<MountFd> for OwnedFd {
    fn from(mountfd: MountFd) -> Self {
        mountfd.0
    }
}

/// Moves a mount.
#[allow(non_camel_case_types)]
pub fn move_mount<F_Fd: AsFd, F_P: AsRef<Path>, T_Fd: AsFd, T_P: AsRef<Path>>(
    from_dfd: F_Fd,
    from_pathname: F_P,
    to_dfd: T_Fd,
    to_pathname: T_P,
    flags: MoveMountFlags,
) -> Result<(), Errno> {
    rustix::mount::move_mount(
        from_dfd.as_fd(),
        from_pathname.as_ref(),
        to_dfd.as_fd(),
        to_pathname.as_ref(),
        flags.as_rustix(),
    )?;

    Ok(())
}

/// Enumeration of Mount Propagation
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
#[repr(u64)]
pub enum Propagation {
    /// Private
    Private = sys::MS_PRIVATE,
    /// Shared
    Shared = sys::MS_SHARED,
    /// Slave
    Slave = sys::MS_SLAVE,
    /// Unbindable
    Unbindable = sys::MS_UNBINDABLE,
}
impl Propagation {
    fn to_sys(self) -> u64 {
        self as _
    }
}

/// `MountAttr`
#[derive(Debug)]
#[repr(transparent)]
pub struct MountAttr(sys::mount_attr);
impl MountAttr {
    /// Creates a new empty `MountAttr`.
    pub fn new() -> Self {
        // SAFETY: sys::mount_attr is a struct of integers only.
        Self(unsafe { std::mem::zeroed() })
    }

    /// Adds `attr_flags` to the set of flags that should be set.
    pub fn set_flags(&mut self, attr_flags: MountAttrFlags) -> &mut Self {
        self.0.attr_set |= u64::from(attr_flags.bits()) & !sys::MOUNT_ATTR_IDMAP;
        self
    }

    /// Adds `attr_flags` to the set of flags that should be cleared.
    pub fn clear_flags(&mut self, attr_flags: MountAttrFlags) -> &mut Self {
        self.0.attr_clr |= u64::from(attr_flags.bits());
        self
    }

    /// Changes the mount propagation to `propagation`.
    pub fn propagation(&mut self, propagation: Propagation) -> &mut Self {
        self.0.propagation = propagation.to_sys();
        self
    }

    // TODO: ID-mapped mounts
}
impl Default for MountAttr {
    fn default() -> Self {
        Self::new()
    }
}

/// Changes properties of a mount or mount tree.
pub fn mount_setattr<Fd: AsFd, P: AsRef<Path>>(
    dirfd: Fd,
    pathname: P,
    flags: MountSetattrFlags,
    attr: &mut MountAttr,
) -> Result<(), Errno> {
    let pathname = CString::new(pathname.as_ref().as_bytes())?;

    cvt(unsafe {
        sys::mount_setattr(
            dirfd.as_fd().as_raw_fd(),
            pathname.as_ptr(),
            flags.bits(),
            attr as *mut MountAttr as *mut sys::mount_attr,
            size_of::<sys::mount_attr>(),
        )
    })?;

    Ok(())
}

#[allow(dead_code)]
#[allow(nonstandard_style)]
mod sys {
    use crate::std_suppl::ffi::ctypes::*;

    pub(super) const FSOPEN_CLOEXEC: c_uint = 0x00000001;

    pub(super) const FSPICK_CLOEXEC: c_uint = 0x00000001;
    pub(super) const FSPICK_SYMLINK_NOFOLLOW: c_uint = 0x00000002;
    pub(super) const FSPICK_NO_AUTOMOUNT: c_uint = 0x00000004;
    pub(super) const FSPICK_EMPTY_PATH: c_uint = 0x00000008;

    pub(super) const FSCONFIG_SET_FLAG: c_uint = 0;
    pub(super) const FSCONFIG_SET_STRING: c_uint = 1;
    pub(super) const FSCONFIG_SET_BINARY: c_uint = 2;
    pub(super) const FSCONFIG_SET_PATH: c_uint = 3;
    pub(super) const FSCONFIG_SET_PATH_EMPTY: c_uint = 4;
    pub(super) const FSCONFIG_SET_FD: c_uint = 5;
    pub(super) const FSCONFIG_CMD_CREATE: c_uint = 6;
    pub(super) const FSCONFIG_CMD_RECONFIGURE: c_uint = 7;
    pub(super) const FSCONFIG_CMD_CREATE_EXCL: c_uint = 8;

    pub(super) const FSMOUNT_CLOEXEC: c_uint = 0x00000001;

    pub(super) const MOUNT_ATTR_RDONLY: c_uint = 0x00000001;
    pub(super) const MOUNT_ATTR_NOSUID: c_uint = 0x00000002;
    pub(super) const MOUNT_ATTR_NODEV: c_uint = 0x00000004;
    pub(super) const MOUNT_ATTR_NOEXEC: c_uint = 0x00000008;
    pub(super) const MOUNT_ATTR__ATIME: c_uint = 0x00000070;
    pub(super) const MOUNT_ATTR_RELATIME: c_uint = 0x00000000;
    pub(super) const MOUNT_ATTR_NOATIME: c_uint = 0x00000010;
    pub(super) const MOUNT_ATTR_STRICTATIME: c_uint = 0x00000020;
    pub(super) const MOUNT_ATTR_NODIRATIME: c_uint = 0x00000080;
    pub(super) const MOUNT_ATTR_NOSYMFOLLOW: c_uint = 0x00200000;

    pub(super) const MOUNT_ATTR_IDMAP: c_ulong = 0x00100000;

    pub(super) const MS_UNBINDABLE: c_ulong = 1 << 17;
    pub(super) const MS_PRIVATE: c_ulong = 1 << 18;
    pub(super) const MS_SLAVE: c_ulong = 1 << 19;
    pub(super) const MS_SHARED: c_ulong = 1 << 20;

    pub(super) const OPEN_TREE_CLONE: c_uint = 1;
    pub(super) const OPEN_TREE_CLOEXEC: c_uint = libc::O_CLOEXEC as u32;

    pub(super) const MOVE_MOUNT_F_SYMLINKS: c_uint = 0x00000001;
    pub(super) const MOVE_MOUNT_F_AUTOMOUNTS: c_uint = 0x00000002;
    pub(super) const MOVE_MOUNT_F_EMPTY_PATH: c_uint = 0x00000004;
    pub(super) const MOVE_MOUNT_T_SYMLINKS: c_uint = 0x00000010;
    pub(super) const MOVE_MOUNT_T_AUTOMOUNTS: c_uint = 0x00000020;
    pub(super) const MOVE_MOUNT_T_EMPTY_PATH: c_uint = 0x00000040;
    pub(super) const MOVE_MOUNT_SET_GROUP: c_uint = 0x00000100;
    pub(super) const MOVE_MOUNT_BENEATH: c_uint = 0x00000200;

    #[derive(Debug)]
    #[repr(C)]
    pub(super) struct mount_attr {
        pub(super) attr_set: u64,
        pub(super) attr_clr: u64,
        pub(super) propagation: u64,
        pub(super) userns_fd: u64,
    }
    pub(super) const MOUNT_ATTR_SIZE_VER0: usize = 32;

    unsafe extern "C" {
        pub(super) fn fsopen(fs_name: *const c_char, flags: c_uint) -> c_int;

        pub(super) fn fspick(dfd: c_int, path: *const c_char, flags: c_uint) -> c_int;

        pub(super) fn fsconfig(
            fd: c_int,
            cmd: c_uint,
            key: *const c_char,
            value: *const c_void,
            aux: c_int,
        ) -> c_int;

        pub(super) fn fsmount(fd: c_int, flags: c_uint, attr_flags: c_uint) -> c_int;

        pub(super) fn open_tree(dfd: c_int, filename: *const c_char, flags: c_uint) -> c_int;

        pub(super) fn move_mount(
            from_dfd: c_int,
            from_pathname: *const c_char,
            to_dfd: c_int,
            to_pathname: *const c_char,
            flags: c_uint,
        ) -> c_int;

        pub(super) fn mount_setattr(
            dfd: c_int,
            path: *const c_char,
            flags: c_uint,
            uattr: *mut mount_attr,
            usize: c_size_t,
        ) -> c_int;
    }
}

#[cfg(target_env = "musl")]
#[allow(non_camel_case_types)]
mod musl_polyfill {
    use core::ffi::*;

    // feature(c_size_t) is unstable
    type c_size_t = usize;

    #[unsafe(no_mangle)]
    unsafe extern "C" fn fsopen(fs_name: *const c_char, flags: c_uint) -> c_int {
        unsafe { libc::syscall(libc::SYS_fsopen, fs_name, flags) as c_int }
    }

    #[unsafe(no_mangle)]
    unsafe extern "C" fn fspick(dfd: c_int, path: *const c_char, flags: c_uint) -> c_int {
        unsafe { libc::syscall(libc::SYS_fspick, dfd, path, flags) as c_int }
    }

    #[unsafe(no_mangle)]
    unsafe extern "C" fn fsconfig(
        fd: c_int,
        cmd: c_uint,
        key: *const c_char,
        value: *const c_void,
        aux: c_int,
    ) -> c_int {
        unsafe { libc::syscall(libc::SYS_fsconfig, fd, cmd, key, value, aux) as c_int }
    }

    #[unsafe(no_mangle)]
    unsafe extern "C" fn fsmount(fd: c_int, flags: c_uint, attr_flags: c_uint) -> c_int {
        unsafe { libc::syscall(libc::SYS_fsmount, fd, flags, attr_flags) as c_int }
    }

    #[unsafe(no_mangle)]
    unsafe extern "C" fn open_tree(dfd: c_int, filename: *const c_char, flags: c_uint) -> c_int {
        unsafe { libc::syscall(libc::SYS_open_tree, dfd, filename, flags) as c_int }
    }

    #[unsafe(no_mangle)]
    unsafe extern "C" fn move_mount(
        from_dfd: c_int,
        from_pathname: *const c_char,
        to_dfd: c_int,
        to_pathname: *const c_char,
        flags: c_uint,
    ) -> c_int {
        unsafe {
            libc::syscall(
                libc::SYS_move_mount,
                from_dfd,
                from_pathname,
                to_dfd,
                to_pathname,
                flags,
            ) as c_int
        }
    }

    #[unsafe(no_mangle)]
    unsafe extern "C" fn mount_setattr(
        dfd: c_int,
        path: *const c_char,
        flags: c_uint,
        uattr: *mut super::sys::mount_attr,
        usize: c_size_t,
    ) -> c_int {
        unsafe { libc::syscall(libc::SYS_mount_setattr, dfd, path, flags, uattr, usize) as c_int }
    }
}
