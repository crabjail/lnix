// SPDX-License-Identifier: Apache-2.0 OR MIT

/*
 * Copyright © 2022 The lnix Authors
 *
 * Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
 * https://www.apache.org/licenses/LICENSE-2.0> or the MIT license
 * <LICENSE-MIT or https://opensource.org/licenses/MIT>, at your
 * option. This file may not be copied, modified, or distributed
 * except according to those terms.
 */

use std::ffi::{CString, OsStr};
use std::os::unix::prelude::*;

use crate::error::{Errno, cvt};

/// Changes the root mount.
#[deprecated(note = "Use rustix::process::pivot_root")]
pub fn pivot_root<N, O>(new_root: &N, put_old: &O) -> Result<(), Errno>
where
    N: AsRef<OsStr> + ?Sized,
    O: AsRef<OsStr> + ?Sized,
{
    let new_root = CString::new(new_root.as_ref().as_bytes())?;
    let put_old = CString::new(put_old.as_ref().as_bytes())?;

    cvt(
        unsafe { libc::syscall(libc::SYS_pivot_root, new_root.as_ptr(), put_old.as_ptr()) } as i32,
    )?;

    Ok(())
}
