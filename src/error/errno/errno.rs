// SPDX-License-Identifier: Apache-2.0 OR MIT

/*
 * Copyright © 2023 The lnix Authors
 *
 * Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
 * https://www.apache.org/licenses/LICENSE-2.0> or the MIT license
 * <LICENSE-MIT or https://opensource.org/licenses/MIT>, at your
 * option. This file may not be copied, modified, or distributed
 * except according to those terms.
 */

use core::fmt;

/// Error type representing Operating-system errors.
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[non_exhaustive]
#[repr(i32)]
pub enum Errno {
    /// Operation not permitted
    EPERM = 1,
    /// No such file or directory
    ENOENT = 2,
    /// No such process
    ESRCH = 3,
    /// Interrupted system call
    EINTR = 4,
    /// Input/output error
    EIO = 5,
    /// No such device or address
    ENXIO = 6,
    /// Argument list too long
    E2BIG = 7,
    /// Exec format error
    ENOEXEC = 8,
    /// Bad file descriptor
    EBADF = 9,
    /// No child processes
    ECHILD = 10,
    /// Resource temporarily unavailable
    EAGAIN = 11,
    /// Cannot allocate memory
    ENOMEM = 12,
    /// Permission denied
    EACCES = 13,
    /// Bad address
    EFAULT = 14,
    /// Block device required
    ENOTBLK = 15,
    /// Device or resource busy
    EBUSY = 16,
    /// File exists
    EEXIST = 17,
    /// Invalid cross-device link
    EXDEV = 18,
    /// No such device
    ENODEV = 19,
    /// Not a directory
    ENOTDIR = 20,
    /// Is a directory
    EISDIR = 21,
    /// Invalid argument
    EINVAL = 22,
    /// Too many open files in system
    ENFILE = 23,
    /// Too many open files
    EMFILE = 24,
    /// Inappropriate ioctl for device
    ENOTTY = 25,
    /// Text file busy
    ETXTBSY = 26,
    /// File too large
    EFBIG = 27,
    /// No space left on device
    ENOSPC = 28,
    /// Illegal seek
    ESPIPE = 29,
    /// Read-only file system
    EROFS = 30,
    /// Too many links
    EMLINK = 31,
    /// Broken pipe
    EPIPE = 32,
    /// Numerical argument out of domain
    EDOM = 33,
    /// Numerical result out of range
    ERANGE = 34,
    /// Resource deadlock avoided
    EDEADLK = 35,
    /// File name too long
    ENAMETOOLONG = 36,
    /// No locks available
    ENOLCK = 37,
    /// Function not implemented
    ENOSYS = 38,
    /// Directory not empty
    ENOTEMPTY = 39,
    /// Too many levels of symbolic links
    ELOOP = 40,
    /// No message of desired type
    ENOMSG = 42,
    /// Identifier removed
    EIDRM = 43,
    /// Channel number out of range
    ECHRNG = 44,
    /// Level 2 not synchronized
    EL2NSYNC = 45,
    /// Level 3 halted
    EL3HLT = 46,
    /// Level 3 reset
    EL3RST = 47,
    /// Link number out of range
    ELNRNG = 48,
    /// Protocol driver not attached
    EUNATCH = 49,
    /// No CSI structure available
    ENOCSI = 50,
    /// Level 2 halted
    EL2HLT = 51,
    /// Invalid exchange
    EBADE = 52,
    /// Invalid request descriptor
    EBADR = 53,
    /// Exchange full
    EXFULL = 54,
    /// No anode
    ENOANO = 55,
    /// Invalid request code
    EBADRQC = 56,
    /// Invalid slot
    EBADSLT = 57,
    /// Bad font file format
    EBFONT = 59,
    /// Device not a stream
    ENOSTR = 60,
    /// No data available
    ENODATA = 61,
    /// Timer expired
    ETIME = 62,
    /// Out of streams resources
    ENOSR = 63,
    /// Machine is not on the network
    ENONET = 64,
    /// Package not installed
    ENOPKG = 65,
    /// Object is remote
    EREMOTE = 66,
    /// Link has been severed
    ENOLINK = 67,
    /// Advertise error
    EADV = 68,
    /// Srmount error
    ESRMNT = 69,
    /// Communication error on send
    ECOMM = 70,
    /// Protocol error
    EPROTO = 71,
    /// Multihop attempted
    EMULTIHOP = 72,
    /// RFS specific error
    EDOTDOT = 73,
    /// Bad message
    EBADMSG = 74,
    /// Value too large for defined data type
    EOVERFLOW = 75,
    /// Name not unique on network
    ENOTUNIQ = 76,
    /// File descriptor in bad state
    EBADFD = 77,
    /// Remote address changed
    EREMCHG = 78,
    /// Can not access a needed shared library
    ELIBACC = 79,
    /// Accessing a corrupted shared library
    ELIBBAD = 80,
    /// .lib section in a.out corrupted
    ELIBSCN = 81,
    /// Attempting to link in too many shared libraries
    ELIBMAX = 82,
    /// Cannot exec a shared library directly
    ELIBEXEC = 83,
    /// Invalid or incomplete multibyte or wide character
    EILSEQ = 84,
    /// Interrupted system call should be restarted
    ERESTART = 85,
    /// Streams pipe error
    ESTRPIPE = 86,
    /// Too many users
    EUSERS = 87,
    /// Socket operation on non-socket
    ENOTSOCK = 88,
    /// Destination address required
    EDESTADDRREQ = 89,
    /// Message too long
    EMSGSIZE = 90,
    /// Protocol wrong type for socket
    EPROTOTYPE = 91,
    /// Protocol not available
    ENOPROTOOPT = 92,
    /// Protocol not supported
    EPROTONOSUPPORT = 93,
    /// Socket type not supported
    ESOCKTNOSUPPORT = 94,
    /// Operation not supported
    EOPNOTSUPP = 95,
    /// Protocol family not supported
    EPFNOSUPPORT = 96,
    /// Address family not supported by protocol
    EAFNOSUPPORT = 97,
    /// Address already in use
    EADDRINUSE = 98,
    /// Cannot assign requested address
    EADDRNOTAVAIL = 99,
    /// Network is down
    ENETDOWN = 100,
    /// Network is unreachable
    ENETUNREACH = 101,
    /// Network dropped connection on reset
    ENETRESET = 102,
    /// Software caused connection abort
    ECONNABORTED = 103,
    /// Connection reset by peer
    ECONNRESET = 104,
    /// No buffer space available
    ENOBUFS = 105,
    /// Transport endpoint is already connected
    EISCONN = 106,
    /// Transport endpoint is not connected
    ENOTCONN = 107,
    /// Cannot send after transport endpoint shutdown
    ESHUTDOWN = 108,
    /// Too many references: cannot splice
    ETOOMANYREFS = 109,
    /// Connection timed out
    ETIMEDOUT = 110,
    /// Connection refused
    ECONNREFUSED = 111,
    /// Host is down
    EHOSTDOWN = 112,
    /// No route to host
    EHOSTUNREACH = 113,
    /// Operation already in progress
    EALREADY = 114,
    /// Operation now in progress
    EINPROGRESS = 115,
    /// Stale file handle
    ESTALE = 116,
    /// Structure needs cleaning
    EUCLEAN = 117,
    /// Not a XENIX named type file
    ENOTNAM = 118,
    /// No XENIX semaphores available
    ENAVAIL = 119,
    /// Is a named type file
    EISNAM = 120,
    /// Remote I/O error
    EREMOTEIO = 121,
    /// Disk quota exceeded
    EDQUOT = 122,
    /// No medium found
    ENOMEDIUM = 123,
    /// Wrong medium type
    EMEDIUMTYPE = 124,
    /// Operation canceled
    ECANCELED = 125,
    /// Required key not available
    ENOKEY = 126,
    /// Key has expired
    EKEYEXPIRED = 127,
    /// Key has been revoked
    EKEYREVOKED = 128,
    /// Key was rejected by service
    EKEYREJECTED = 129,
    /// Owner died
    EOWNERDEAD = 130,
    /// State not recoverable
    ENOTRECOVERABLE = 131,
    /// Operation not possible due to RF-kill
    ERFKILL = 132,
    /// Memory page has hardware error
    EHWPOISON = 133,
}
impl Errno {
    /// Converts from a raw os value to `Errno`.
    #[allow(clippy::too_many_lines)]
    pub const fn from_raw(errnum: i32) -> Option<Self> {
        match errnum {
            1 => Some(Self::EPERM),
            2 => Some(Self::ENOENT),
            3 => Some(Self::ESRCH),
            4 => Some(Self::EINTR),
            5 => Some(Self::EIO),
            6 => Some(Self::ENXIO),
            7 => Some(Self::E2BIG),
            8 => Some(Self::ENOEXEC),
            9 => Some(Self::EBADF),
            10 => Some(Self::ECHILD),
            11 => Some(Self::EAGAIN),
            12 => Some(Self::ENOMEM),
            13 => Some(Self::EACCES),
            14 => Some(Self::EFAULT),
            15 => Some(Self::ENOTBLK),
            16 => Some(Self::EBUSY),
            17 => Some(Self::EEXIST),
            18 => Some(Self::EXDEV),
            19 => Some(Self::ENODEV),
            20 => Some(Self::ENOTDIR),
            21 => Some(Self::EISDIR),
            22 => Some(Self::EINVAL),
            23 => Some(Self::ENFILE),
            24 => Some(Self::EMFILE),
            25 => Some(Self::ENOTTY),
            26 => Some(Self::ETXTBSY),
            27 => Some(Self::EFBIG),
            28 => Some(Self::ENOSPC),
            29 => Some(Self::ESPIPE),
            30 => Some(Self::EROFS),
            31 => Some(Self::EMLINK),
            32 => Some(Self::EPIPE),
            33 => Some(Self::EDOM),
            34 => Some(Self::ERANGE),
            35 => Some(Self::EDEADLK),
            36 => Some(Self::ENAMETOOLONG),
            37 => Some(Self::ENOLCK),
            38 => Some(Self::ENOSYS),
            39 => Some(Self::ENOTEMPTY),
            40 => Some(Self::ELOOP),
            42 => Some(Self::ENOMSG),
            43 => Some(Self::EIDRM),
            44 => Some(Self::ECHRNG),
            45 => Some(Self::EL2NSYNC),
            46 => Some(Self::EL3HLT),
            47 => Some(Self::EL3RST),
            48 => Some(Self::ELNRNG),
            49 => Some(Self::EUNATCH),
            50 => Some(Self::ENOCSI),
            51 => Some(Self::EL2HLT),
            52 => Some(Self::EBADE),
            53 => Some(Self::EBADR),
            54 => Some(Self::EXFULL),
            55 => Some(Self::ENOANO),
            56 => Some(Self::EBADRQC),
            57 => Some(Self::EBADSLT),
            59 => Some(Self::EBFONT),
            60 => Some(Self::ENOSTR),
            61 => Some(Self::ENODATA),
            62 => Some(Self::ETIME),
            63 => Some(Self::ENOSR),
            64 => Some(Self::ENONET),
            65 => Some(Self::ENOPKG),
            66 => Some(Self::EREMOTE),
            67 => Some(Self::ENOLINK),
            68 => Some(Self::EADV),
            69 => Some(Self::ESRMNT),
            70 => Some(Self::ECOMM),
            71 => Some(Self::EPROTO),
            72 => Some(Self::EMULTIHOP),
            73 => Some(Self::EDOTDOT),
            74 => Some(Self::EBADMSG),
            75 => Some(Self::EOVERFLOW),
            76 => Some(Self::ENOTUNIQ),
            77 => Some(Self::EBADFD),
            78 => Some(Self::EREMCHG),
            79 => Some(Self::ELIBACC),
            80 => Some(Self::ELIBBAD),
            81 => Some(Self::ELIBSCN),
            82 => Some(Self::ELIBMAX),
            83 => Some(Self::ELIBEXEC),
            84 => Some(Self::EILSEQ),
            85 => Some(Self::ERESTART),
            86 => Some(Self::ESTRPIPE),
            87 => Some(Self::EUSERS),
            88 => Some(Self::ENOTSOCK),
            89 => Some(Self::EDESTADDRREQ),
            90 => Some(Self::EMSGSIZE),
            91 => Some(Self::EPROTOTYPE),
            92 => Some(Self::ENOPROTOOPT),
            93 => Some(Self::EPROTONOSUPPORT),
            94 => Some(Self::ESOCKTNOSUPPORT),
            95 => Some(Self::EOPNOTSUPP),
            96 => Some(Self::EPFNOSUPPORT),
            97 => Some(Self::EAFNOSUPPORT),
            98 => Some(Self::EADDRINUSE),
            99 => Some(Self::EADDRNOTAVAIL),
            100 => Some(Self::ENETDOWN),
            101 => Some(Self::ENETUNREACH),
            102 => Some(Self::ENETRESET),
            103 => Some(Self::ECONNABORTED),
            104 => Some(Self::ECONNRESET),
            105 => Some(Self::ENOBUFS),
            106 => Some(Self::EISCONN),
            107 => Some(Self::ENOTCONN),
            108 => Some(Self::ESHUTDOWN),
            109 => Some(Self::ETOOMANYREFS),
            110 => Some(Self::ETIMEDOUT),
            111 => Some(Self::ECONNREFUSED),
            112 => Some(Self::EHOSTDOWN),
            113 => Some(Self::EHOSTUNREACH),
            114 => Some(Self::EALREADY),
            115 => Some(Self::EINPROGRESS),
            116 => Some(Self::ESTALE),
            117 => Some(Self::EUCLEAN),
            118 => Some(Self::ENOTNAM),
            119 => Some(Self::ENAVAIL),
            120 => Some(Self::EISNAM),
            121 => Some(Self::EREMOTEIO),
            122 => Some(Self::EDQUOT),
            123 => Some(Self::ENOMEDIUM),
            124 => Some(Self::EMEDIUMTYPE),
            125 => Some(Self::ECANCELED),
            126 => Some(Self::ENOKEY),
            127 => Some(Self::EKEYEXPIRED),
            128 => Some(Self::EKEYREVOKED),
            129 => Some(Self::EKEYREJECTED),
            130 => Some(Self::EOWNERDEAD),
            131 => Some(Self::ENOTRECOVERABLE),
            132 => Some(Self::ERFKILL),
            133 => Some(Self::EHWPOISON),
            _ => None,
        }
    }
}
impl fmt::Display for Errno {
    #[allow(clippy::too_many_lines)]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let description = match self {
            Self::EPERM => "EPERM 1 Operation not permitted",
            Self::ENOENT => "ENOENT 2 No such file or directory",
            Self::ESRCH => "ESRCH 3 No such process",
            Self::EINTR => "EINTR 4 Interrupted system call",
            Self::EIO => "EIO 5 Input/output error",
            Self::ENXIO => "ENXIO 6 No such device or address",
            Self::E2BIG => "E2BIG 7 Argument list too long",
            Self::ENOEXEC => "ENOEXEC 8 Exec format error",
            Self::EBADF => "EBADF 9 Bad file descriptor",
            Self::ECHILD => "ECHILD 10 No child processes",
            Self::EAGAIN => "EAGAIN 11 Resource temporarily unavailable",
            Self::ENOMEM => "ENOMEM 12 Cannot allocate memory",
            Self::EACCES => "EACCES 13 Permission denied",
            Self::EFAULT => "EFAULT 14 Bad address",
            Self::ENOTBLK => "ENOTBLK 15 Block device required",
            Self::EBUSY => "EBUSY 16 Device or resource busy",
            Self::EEXIST => "EEXIST 17 File exists",
            Self::EXDEV => "EXDEV 18 Invalid cross-device link",
            Self::ENODEV => "ENODEV 19 No such device",
            Self::ENOTDIR => "ENOTDIR 20 Not a directory",
            Self::EISDIR => "EISDIR 21 Is a directory",
            Self::EINVAL => "EINVAL 22 Invalid argument",
            Self::ENFILE => "ENFILE 23 Too many open files in system",
            Self::EMFILE => "EMFILE 24 Too many open files",
            Self::ENOTTY => "ENOTTY 25 Inappropriate ioctl for device",
            Self::ETXTBSY => "ETXTBSY 26 Text file busy",
            Self::EFBIG => "EFBIG 27 File too large",
            Self::ENOSPC => "ENOSPC 28 No space left on device",
            Self::ESPIPE => "ESPIPE 29 Illegal seek",
            Self::EROFS => "EROFS 30 Read-only file system",
            Self::EMLINK => "EMLINK 31 Too many links",
            Self::EPIPE => "EPIPE 32 Broken pipe",
            Self::EDOM => "EDOM 33 Numerical argument out of domain",
            Self::ERANGE => "ERANGE 34 Numerical result out of range",
            Self::EDEADLK => "EDEADLK 35 Resource deadlock avoided",
            Self::ENAMETOOLONG => "ENAMETOOLONG 36 File name too long",
            Self::ENOLCK => "ENOLCK 37 No locks available",
            Self::ENOSYS => "ENOSYS 38 Function not implemented",
            Self::ENOTEMPTY => "ENOTEMPTY 39 Directory not empty",
            Self::ELOOP => "ELOOP 40 Too many levels of symbolic links",
            Self::ENOMSG => "ENOMSG 42 No message of desired type",
            Self::EIDRM => "EIDRM 43 Identifier removed",
            Self::ECHRNG => "ECHRNG 44 Channel number out of range",
            Self::EL2NSYNC => "EL2NSYNC 45 Level 2 not synchronized",
            Self::EL3HLT => "EL3HLT 46 Level 3 halted",
            Self::EL3RST => "EL3RST 47 Level 3 reset",
            Self::ELNRNG => "ELNRNG 48 Link number out of range",
            Self::EUNATCH => "EUNATCH 49 Protocol driver not attached",
            Self::ENOCSI => "ENOCSI 50 No CSI structure available",
            Self::EL2HLT => "EL2HLT 51 Level 2 halted",
            Self::EBADE => "EBADE 52 Invalid exchange",
            Self::EBADR => "EBADR 53 Invalid request descriptor",
            Self::EXFULL => "EXFULL 54 Exchange full",
            Self::ENOANO => "ENOANO 55 No anode",
            Self::EBADRQC => "EBADRQC 56 Invalid request code",
            Self::EBADSLT => "EBADSLT 57 Invalid slot",
            Self::EBFONT => "EBFONT 59 Bad font file format",
            Self::ENOSTR => "ENOSTR 60 Device not a stream",
            Self::ENODATA => "ENODATA 61 No data available",
            Self::ETIME => "ETIME 62 Timer expired",
            Self::ENOSR => "ENOSR 63 Out of streams resources",
            Self::ENONET => "ENONET 64 Machine is not on the network",
            Self::ENOPKG => "ENOPKG 65 Package not installed",
            Self::EREMOTE => "EREMOTE 66 Object is remote",
            Self::ENOLINK => "ENOLINK 67 Link has been severed",
            Self::EADV => "EADV 68 Advertise error",
            Self::ESRMNT => "ESRMNT 69 Srmount error",
            Self::ECOMM => "ECOMM 70 Communication error on send",
            Self::EPROTO => "EPROTO 71 Protocol error",
            Self::EMULTIHOP => "EMULTIHOP 72 Multihop attempted",
            Self::EDOTDOT => "EDOTDOT 73 RFS specific error",
            Self::EBADMSG => "EBADMSG 74 Bad message",
            Self::EOVERFLOW => "EOVERFLOW 75 Value too large for defined data type",
            Self::ENOTUNIQ => "ENOTUNIQ 76 Name not unique on network",
            Self::EBADFD => "EBADFD 77 File descriptor in bad state",
            Self::EREMCHG => "EREMCHG 78 Remote address changed",
            Self::ELIBACC => "ELIBACC 79 Can not access a needed shared library",
            Self::ELIBBAD => "ELIBBAD 80 Accessing a corrupted shared library",
            Self::ELIBSCN => "ELIBSCN 81 .lib section in a.out corrupted",
            Self::ELIBMAX => "ELIBMAX 82 Attempting to link in too many shared libraries",
            Self::ELIBEXEC => "ELIBEXEC 83 Cannot exec a shared library directly",
            Self::EILSEQ => "EILSEQ 84 Invalid or incomplete multibyte or wide character",
            Self::ERESTART => "ERESTART 85 Interrupted system call should be restarted",
            Self::ESTRPIPE => "ESTRPIPE 86 Streams pipe error",
            Self::EUSERS => "EUSERS 87 Too many users",
            Self::ENOTSOCK => "ENOTSOCK 88 Socket operation on non-socket",
            Self::EDESTADDRREQ => "EDESTADDRREQ 89 Destination address required",
            Self::EMSGSIZE => "EMSGSIZE 90 Message too long",
            Self::EPROTOTYPE => "EPROTOTYPE 91 Protocol wrong type for socket",
            Self::ENOPROTOOPT => "ENOPROTOOPT 92 Protocol not available",
            Self::EPROTONOSUPPORT => "EPROTONOSUPPORT 93 Protocol not supported",
            Self::ESOCKTNOSUPPORT => "ESOCKTNOSUPPORT 94 Socket type not supported",
            Self::EOPNOTSUPP => "EOPNOTSUPP 95 Operation not supported",
            Self::EPFNOSUPPORT => "EPFNOSUPPORT 96 Protocol family not supported",
            Self::EAFNOSUPPORT => "EAFNOSUPPORT 97 Address family not supported by protocol",
            Self::EADDRINUSE => "EADDRINUSE 98 Address already in use",
            Self::EADDRNOTAVAIL => "EADDRNOTAVAIL 99 Cannot assign requested address",
            Self::ENETDOWN => "ENETDOWN 100 Network is down",
            Self::ENETUNREACH => "ENETUNREACH 101 Network is unreachable",
            Self::ENETRESET => "ENETRESET 102 Network dropped connection on reset",
            Self::ECONNABORTED => "ECONNABORTED 103 Software caused connection abort",
            Self::ECONNRESET => "ECONNRESET 104 Connection reset by peer",
            Self::ENOBUFS => "ENOBUFS 105 No buffer space available",
            Self::EISCONN => "EISCONN 106 Transport endpoint is already connected",
            Self::ENOTCONN => "ENOTCONN 107 Transport endpoint is not connected",
            Self::ESHUTDOWN => "ESHUTDOWN 108 Cannot send after transport endpoint shutdown",
            Self::ETOOMANYREFS => "ETOOMANYREFS 109 Too many references: cannot splice",
            Self::ETIMEDOUT => "ETIMEDOUT 110 Connection timed out",
            Self::ECONNREFUSED => "ECONNREFUSED 111 Connection refused",
            Self::EHOSTDOWN => "EHOSTDOWN 112 Host is down",
            Self::EHOSTUNREACH => "EHOSTUNREACH 113 No route to host",
            Self::EALREADY => "EALREADY 114 Operation already in progress",
            Self::EINPROGRESS => "EINPROGRESS 115 Operation now in progress",
            Self::ESTALE => "ESTALE 116 Stale file handle",
            Self::EUCLEAN => "EUCLEAN 117 Structure needs cleaning",
            Self::ENOTNAM => "ENOTNAM 118 Not a XENIX named type file",
            Self::ENAVAIL => "ENAVAIL 119 No XENIX semaphores available",
            Self::EISNAM => "EISNAM 120 Is a named type file",
            Self::EREMOTEIO => "EREMOTEIO 121 Remote I/O error",
            Self::EDQUOT => "EDQUOT 122 Disk quota exceeded",
            Self::ENOMEDIUM => "ENOMEDIUM 123 No medium found",
            Self::EMEDIUMTYPE => "EMEDIUMTYPE 124 Wrong medium type",
            Self::ECANCELED => "ECANCELED 125 Operation canceled",
            Self::ENOKEY => "ENOKEY 126 Required key not available",
            Self::EKEYEXPIRED => "EKEYEXPIRED 127 Key has expired",
            Self::EKEYREVOKED => "EKEYREVOKED 128 Key has been revoked",
            Self::EKEYREJECTED => "EKEYREJECTED 129 Key was rejected by service",
            Self::EOWNERDEAD => "EOWNERDEAD 130 Owner died",
            Self::ENOTRECOVERABLE => "ENOTRECOVERABLE 131 State not recoverable",
            Self::ERFKILL => "ERFKILL 132 Operation not possible due to RF-kill",
            Self::EHWPOISON => "EHWPOISON 133 Memory page has hardware error",
        };
        f.write_str(description)
    }
}
