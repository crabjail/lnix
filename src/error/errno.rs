// SPDX-License-Identifier: Apache-2.0 OR MIT

/*
 * Copyright © 2022 The lnix Authors
 *
 * Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
 * https://www.apache.org/licenses/LICENSE-2.0> or the MIT license
 * <LICENSE-MIT or https://opensource.org/licenses/MIT>, at your
 * option. This file may not be copied, modified, or distributed
 * except according to those terms.
 */

#![allow(clippy::upper_case_acronyms)] // FIXME: Rename and Remove

// FIXME: Move errno definition here, but use a macro like crablock/seccomp.
#[allow(clippy::module_inception)]
mod errno;
pub use errno::Errno;

// Errno:
//   LC_ALL=C errno -l | grep -vE "(EWOULDBLOCK|EDEADLOCK|ENOTSUP)" | sed -E 's/([A-Z0-9]+) ([0-9]+) (.+)/    \/\/\/ \3\n    \1 = \2,/'
// Errno::from_raw:
//   LC_ALL=C errno -l | grep -vE "(EWOULDBLOCK|EDEADLOCK|ENOTSUP)" | sed -E 's/([A-Z0-9]+) ([0-9]+) (.+)/            \2 => Some(Self::\1),/'
// <Errno as Display>::fmt:
//   LC_ALL=C errno -l | grep -vE "(EWOULDBLOCK|EDEADLOCK|ENOTSUP)" | sed -E 's/([A-Z0-9]+) ([0-9]+) (.+)/            Self::\1 => "\1 \2 \3",/'

#[allow(missing_docs)]
impl Errno {
    pub const EWOULDBLOCK: Self = Self::EAGAIN;
    pub const EDEADLOCK: Self = Self::EDEADLK;
    pub const ENOTSUP: Self = Self::EOPNOTSUPP;
}
impl Errno {
    /// Gets the os `errno` of the current thread.
    pub fn os_errno() -> Result<Self, i32> {
        let errnum = unsafe { *libc::__errno_location() };
        match Self::from_raw(errnum) {
            Some(errno) => Ok(errno),
            None => Err(errnum),
        }
    }

    /// Sets the os `errno` of the current thread.
    pub fn set_os_errno(self) {
        unsafe {
            *libc::__errno_location() = self.to_raw();
        }
    }

    /// Converts this `Errno` to the raw os value.
    ///
    /// This is equivalent to an enum-to-integer cast.
    #[must_use]
    pub const fn to_raw(self) -> i32 {
        self as i32
    }
}
impl std::error::Error for Errno {}
impl From<rustix::io::Errno> for Errno {
    fn from(rustix_errno: rustix::io::Errno) -> Self {
        Self::from_raw(rustix_errno.raw_os_error()).unwrap()
    }
}
#[cfg(feature = "capctl")]
impl From<capctl::Error> for Errno {
    fn from(capctl_error: capctl::Error) -> Self {
        Self::from_raw(capctl_error.code()).unwrap()
    }
}
impl From<std::ffi::NulError> for Errno {
    fn from(_: std::ffi::NulError) -> Self {
        Self::EINVAL
    }
}
impl From<Errno> for std::io::Error {
    fn from(errno: Errno) -> Self {
        Self::from_raw_os_error(errno as i32)
    }
}
