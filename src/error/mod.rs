// SPDX-License-Identifier: Apache-2.0 OR MIT

/*
 * Copyright © 2022 The lnix Authors
 *
 * Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
 * https://www.apache.org/licenses/LICENSE-2.0> or the MIT license
 * <LICENSE-MIT or https://opensource.org/licenses/MIT>, at your
 * option. This file may not be copied, modified, or distributed
 * except according to those terms.
 */

//! General error types.

mod errno;
pub use errno::Errno;

pub(crate) fn cvt(r: i32) -> Result<i32, Errno> {
    if r == -1 {
        Err(Errno::os_errno().expect("Errno should contain a definition for"))
    } else {
        Ok(r)
    }
}
