// SPDX-License-Identifier: Apache-2.0 OR MIT

/*
 * Copyright © 2022 The lnix Authors
 *
 * Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
 * https://www.apache.org/licenses/LICENSE-2.0> or the MIT license
 * <LICENSE-MIT or https://opensource.org/licenses/MIT>, at your
 * option. This file may not be copied, modified, or distributed
 * except according to those terms.
 */

//! lnix - Rust friendly bindings to Linux APIs

#![cfg(target_os = "linux")]
#![cfg(any(target_arch = "x86_64", target_arch = "aarch64"))]

pub mod error;

/// Never type because `!` is unstable.
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum Never {}

/// `unreachable_unchecked()` gated by an `unreachable!()` if `debug_assertions` are enabled.
#[macro_export]
macro_rules! release_unreachable_unchecked {
    ($($arg:tt)*) => {{
        #![allow(unreachable_code, reason = "false-positive (if debug_assertions are disabled)")]
        #[cfg(debug_assertions)]
        ::core::unreachable!($($arg)*);
        ::core::hint::unreachable_unchecked();
    }};
}

#[cfg(feature = "fs")]
pub mod fs;
#[cfg(feature = "io")]
pub mod io;
#[cfg(feature = "ioctl")]
pub mod ioctl;
#[cfg(feature = "ipc")]
pub mod ipc;
#[cfg(feature = "mount")]
pub mod mount;
#[cfg(feature = "process")]
pub mod process;
#[cfg(feature = "seccomp")]
pub mod seccomp;
#[cfg(feature = "signal")]
pub mod signal;
#[cfg(feature = "std_suppl")]
pub mod std_suppl;
#[cfg(feature = "ugid")]
pub mod ugid;
