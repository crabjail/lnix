// SPDX-License-Identifier: Apache-2.0 OR MIT

/*
 * Copyright © 2024 The lnix Authors
 *
 * Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
 * https://www.apache.org/licenses/LICENSE-2.0> or the MIT license
 * <LICENSE-MIT or https://opensource.org/licenses/MIT>, at your
 * option. This file may not be copied, modified, or distributed
 * except according to those terms.
 */

//! I/O

use std::os::unix::prelude::*;

use rustix::io::Errno as RustixErrno;
use rustix::ioctl::{BadOpcode, NoArg, ioctl};

#[allow(clippy::upper_case_acronyms)]
type FIONCLEX = BadOpcode<{ linux_raw_sys::ioctl::FIONCLEX }>;
#[allow(clippy::upper_case_acronyms)]
type FIOCLEX = BadOpcode<{ linux_raw_sys::ioctl::FIOCLEX }>;

/// Removes the close-on-exec flag from `fd`.
#[doc(alias = "FIONCLEX")]
pub fn ioctl_fionclex<FD: AsFd>(fd: FD) -> Result<(), RustixErrno> {
    unsafe {
        let ctl = NoArg::<FIONCLEX>::new();
        ioctl(fd, ctl)
    }
}

/// Sets the close-on-exec flag on `fd`.
#[doc(alias = "FIOCLEX")]
pub fn ioctl_fioclex<FD: AsFd>(fd: FD) -> Result<(), RustixErrno> {
    unsafe {
        let ctl = NoArg::<FIOCLEX>::new();
        ioctl(fd, ctl)
    }
}
