// SPDX-License-Identifier: Apache-2.0 OR MIT

/*
 * Copyright © 2022 The lnix Authors
 *
 * Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
 * https://www.apache.org/licenses/LICENSE-2.0> or the MIT license
 * <LICENSE-MIT or https://opensource.org/licenses/MIT>, at your
 * option. This file may not be copied, modified, or distributed
 * except according to those terms.
 */

//! Operate on the secure computing (seccomp) state of the calling process.

use core::ffi::*;
use core::fmt;
use core::marker::PhantomData;
use core::ptr;

use crate::error::{Errno, cvt};

// Define `SECCOMP_SET_MODE_*` constats here because libc does not define them
// for x86_64-unknown-linux-musl target.
const SECCOMP_SET_MODE_STRICT: c_uint = 0;
const SECCOMP_SET_MODE_FILTER: c_uint = 1;

bitflags::bitflags! {
    /// Seccomp filter flags.
    pub struct SeccompFilterFlags: u32 {
        /// Sync filter over all threads.
        const TSYNC = libc::SECCOMP_FILTER_FLAG_TSYNC as u32;
        /// Log all blocked/intercepted syscalls.
        const LOG = libc::SECCOMP_FILTER_FLAG_LOG as u32;
        /// Disable spec store bypass mitigation.
        const SPEC_ALLOW = libc::SECCOMP_FILTER_FLAG_SPEC_ALLOW as u32;
        // const NEW_LISTENER = ...;
        // const TSYNC_ESRCH = ...;
        // const WAIT_KILLABLE_RECV = ...;
    }
}

/// Safe, ABI compatible wrapper around `sock_fprog`.
#[derive(Clone, Copy)]
#[repr(transparent)]
pub struct SockFProg<'a>(libc::sock_fprog, PhantomData<&'a mut [libc::sock_filter]>);
impl<'a> SockFProg<'a> {
    /// Creates a new `SockFProg` for `filter`.
    pub fn new(filter: &'a mut [libc::sock_filter]) -> Self {
        debug_assert!(filter.len() <= c_ushort::MAX as usize);

        Self(
            libc::sock_fprog {
                len: filter.len() as c_ushort,
                filter: filter.as_mut_ptr(),
            },
            PhantomData,
        )
    }
}
impl fmt::Debug for SockFProg<'_> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("SockFProg")
            .field("len", &self.0.len)
            .field("filter", &self.0.filter)
            .finish()
    }
}

unsafe fn seccomp(operation: c_uint, flags: c_uint, args: *mut c_void) -> c_int {
    unsafe { libc::syscall(libc::SYS_seccomp, operation, flags, args, 0, 0, 0) as c_int }
}

/// Confines the calling process[^1] in strict secure computing mode.
///
/// Only `read(2)`, `write(2)`, `_exit(2)` and `sigreturn(2)` may be used,
/// otherwise the process[^1] gets terminated by `SIGKILL`.
///
/// Manpage: `seccomp(2)`
///
/// [^1]: In this context a process is a thread.
pub fn set_mode_strict() -> Result<(), Errno> {
    cvt(unsafe {
        seccomp(
            SECCOMP_SET_MODE_STRICT,
            SeccompFilterFlags::empty().bits(),
            ptr::null_mut(),
        )
    })?;

    Ok(())
}

/// Loads a seccomp filter into the kernel.
///
/// Manpage: `seccomp(2)`
pub fn set_mode_filter(flags: SeccompFilterFlags, args: &SockFProg<'_>) -> Result<(), Errno> {
    if flags.intersects(SeccompFilterFlags::LOG | SeccompFilterFlags::SPEC_ALLOW) {
        unimplemented!("Not yet checked for safety.");
    }

    cvt(unsafe {
        seccomp(
            SECCOMP_SET_MODE_FILTER,
            flags.bits(),
            args as *const _ as *mut c_void,
        )
    })?;

    Ok(())
}
