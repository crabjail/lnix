// SPDX-License-Identifier: Apache-2.0 OR MIT

/*
 * Copyright © 2022 The lnix Authors
 *
 * Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
 * https://www.apache.org/licenses/LICENSE-2.0> or the MIT license
 * <LICENSE-MIT or https://opensource.org/licenses/MIT>, at your
 * option. This file may not be copied, modified, or distributed
 * except according to those terms.
 */

//! Signal operations.

use std::fmt;
use std::mem;
use std::mem::MaybeUninit;
use std::os::unix::prelude::*;
use std::ptr;

use crate::error::{Errno, cvt};

pub use rustix::process::Signal;

/// Signal set.
#[doc(alias = "sigset_t")]
#[repr(transparent)]
#[derive(Clone, Copy)]
pub struct SigSet {
    set: libc::sigset_t,
}
impl SigSet {
    /// Creates a new empty set.
    #[doc(alias = "sigemptyset")]
    pub fn empty() -> Self {
        let mut set = MaybeUninit::<libc::sigset_t>::zeroed();
        let _ = unsafe { libc::sigemptyset(set.as_mut_ptr()) };

        Self {
            set: unsafe { set.assume_init() },
        }
    }

    /// Creates a new full set.
    #[doc(alias = "sigfillset")]
    pub fn fill() -> Self {
        let mut set = MaybeUninit::<libc::sigset_t>::zeroed();
        let _ = unsafe { libc::sigfillset(set.as_mut_ptr()) };

        Self {
            set: unsafe { set.assume_init() },
        }
    }

    /// Adds `signum` from this set.
    #[doc(alias = "sigaddset")]
    pub fn add(&mut self, signum: Signal) {
        let r = unsafe { libc::sigaddset(&mut self.set, signum as i32) };
        debug_assert!(r == 0);
    }

    /// Removes `signum` from this set.
    #[doc(alias = "sigdelset")]
    pub fn del(&mut self, signum: Signal) {
        let r = unsafe { libc::sigdelset(&mut self.set, signum as i32) };
        debug_assert!(r == 0);
    }

    /// Returns `true` if `signum` is in this set.
    #[doc(alias = "sigismember")]
    pub fn ismember(&self, signum: Signal) -> bool {
        let rv = unsafe { libc::sigismember(&self.set, signum as i32) };
        debug_assert!(rv != -1);

        rv == 1
    }
}
impl fmt::Debug for SigSet {
    fn fmt(&self, _f: &mut fmt::Formatter<'_>) -> fmt::Result {
        unimplemented!()
    }
}

/// Type of first argument to [`sigprocmask`].
#[derive(Debug)]
#[repr(i32)]
pub enum SigmaskHow {
    /// `SIG_BLOCK`
    Block = libc::SIG_BLOCK,
    /// `SIG_UNBLOCK`
    Unblock = libc::SIG_UNBLOCK,
    /// `SIG_SETMASK`
    Setmask = libc::SIG_SETMASK,
}

/// Examines and changes blocked signals.
///
/// # Safety
///
/// `sigprocmask` is MT-Unsafe.
pub unsafe fn sigprocmask(how: SigmaskHow, mask: Option<SigSet>) -> Result<SigSet, Errno> {
    let mut oldset = MaybeUninit::<libc::sigset_t>::zeroed();
    let set = if let Some(mask) = mask {
        &mask.set
    } else {
        ptr::null()
    };
    cvt(unsafe { libc::sigprocmask(how as i32, set, oldset.as_mut_ptr()) })?;

    Ok(SigSet {
        set: unsafe { oldset.assume_init() },
    })
}

/// A file descriptor that can be used to accept signals.
#[derive(Debug)]
pub struct SignalFd(OwnedFd);
impl SignalFd {
    /// Creates a new `SignalFd`.
    #[doc(alias = "signalfd")]
    pub fn new(mask: SigSet) -> Result<Self, Errno> {
        let fd = cvt(unsafe { libc::signalfd(-1, &mask.set, libc::SFD_CLOEXEC) })?;

        Ok(Self(unsafe { OwnedFd::from_raw_fd(fd) }))
    }

    /// Reads one pending signal from this `SignalFd`.
    pub fn read(&mut self) -> Result<SignalFdSigInfo, Errno> {
        const SIZE_OF_SIGNALFD_SIGINFO: usize = mem::size_of::<libc::signalfd_siginfo>();

        let mut buf = [0_u8; SIZE_OF_SIGNALFD_SIGINFO];
        let nread = rustix::io::read(&self.0, &mut buf)?;
        assert!(nread == SIZE_OF_SIGNALFD_SIGINFO);

        Ok(
            unsafe {
                mem::transmute::<[u8; SIZE_OF_SIGNALFD_SIGINFO], libc::signalfd_siginfo>(buf)
            },
        )
    }
}
impl AsFd for SignalFd {
    fn as_fd(&self) -> BorrowedFd<'_> {
        self.0.as_fd()
    }
}
impl AsRawFd for SignalFd {
    fn as_raw_fd(&self) -> RawFd {
        self.0.as_raw_fd()
    }
}

/// Return type of [`SignalFd::read`].
pub type SignalFdSigInfo = libc::signalfd_siginfo;
