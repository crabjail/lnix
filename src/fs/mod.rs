// SPDX-License-Identifier: Apache-2.0 OR MIT

/*
 * Copyright © 2022 The lnix Authors
 *
 * Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
 * https://www.apache.org/licenses/LICENSE-2.0> or the MIT license
 * <LICENSE-MIT or https://opensource.org/licenses/MIT>, at your
 * option. This file may not be copied, modified, or distributed
 * except according to those terms.
 */

//! Filesystem operations.

use std::os::unix::prelude::AsFd;
use std::path::Path;

use rustix::fs::{FileType, Mode, mknodat};

use crate::error::Errno;

/// Extension trait for [`FileType`].
pub trait RusixFileTypeExt: Copy {
    /// Returns `true` if this `FileType` is a regular file.
    fn is_file(self) -> bool;
    /// Returns `true` if this `FileType` is a directory.
    fn is_dir(self) -> bool;
    /// Returns `true` if this `FileType` is a symlink.
    fn is_symlink(self) -> bool;
    /// Returns `true` if this `FileType` is a fifo.
    fn is_fifo(self) -> bool;
    /// Returns `true` if this `FileType` is a socket.
    fn is_socket(self) -> bool;
    /// Returns `true` if this `FileType` is a character device.
    fn is_char_device(self) -> bool;
    /// Returns `true` if this `FileType` is a block device.
    fn is_block_device(self) -> bool;
}

impl RusixFileTypeExt for FileType {
    fn is_file(self) -> bool {
        self == FileType::RegularFile
    }
    fn is_dir(self) -> bool {
        self == FileType::Directory
    }
    fn is_symlink(self) -> bool {
        self == FileType::Symlink
    }
    fn is_fifo(self) -> bool {
        self == FileType::Fifo
    }
    fn is_socket(self) -> bool {
        self == FileType::Socket
    }
    fn is_char_device(self) -> bool {
        self == FileType::CharacterDevice
    }
    fn is_block_device(self) -> bool {
        self == FileType::BlockDevice
    }
}

/// Creates a FIFO.
pub fn mkfifoat<FD: AsFd, P: AsRef<Path>>(dirfd: FD, path: P, mode: Mode) -> Result<(), Errno> {
    mknodat(dirfd, path.as_ref(), FileType::Fifo, mode, 0).map_err(Into::into)
}
