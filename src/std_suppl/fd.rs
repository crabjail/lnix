// SPDX-License-Identifier: Apache-2.0 OR MIT

/*
 * Copyright © 2024 The lnix Authors
 *
 * Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
 * https://www.apache.org/licenses/LICENSE-2.0> or the MIT license
 * <LICENSE-MIT or https://opensource.org/licenses/MIT>, at your
 * option. This file may not be copied, modified, or distributed
 * except according to those terms.
 */

//! Supplements for `std::os::fd`.

use std::os::unix::prelude::*;
use std::path::PathBuf;

// pub trait OwnedFdExt {
//     fn openat<FD: AsFd, P: AsRef<Path>>(
//         dirfd: FD,
//         path: P,
//         oflags: OFlags,
//         create_mode: Mode,
//     ) -> IoResult<Self>
//     where
//         Self: Sized;
// }
// impl OwnedFdExt for OwnedFd {
//     fn openat<FD: AsFd, P: AsRef<Path>>(
//         dirfd: FD,
//         path: P,
//         oflags: OFlags,
//         create_mode: Mode,
//     ) -> IoResult<Self> {
//         openat(
//             dirfd.as_fd(),
//             path.as_ref(),
//             oflags | OFlags::CLOEXEC,
//             create_mode,
//         )
//         .map_err(Into::into)
//     }
// }

/// Extension trait for [`OwnedFd`] and [`BorrowedFd`].
pub trait FdExt {
    /// Returns the procfs path of this file-descriptor.
    fn to_procfs_path(&self) -> PathBuf;
}
impl FdExt for OwnedFd {
    fn to_procfs_path(&self) -> PathBuf {
        PathBuf::from(format!("/proc/self/fd/{}", self.as_raw_fd()))
    }
}
impl FdExt for BorrowedFd<'_> {
    fn to_procfs_path(&self) -> PathBuf {
        PathBuf::from(format!("/proc/self/fd/{}", self.as_raw_fd()))
    }
}
