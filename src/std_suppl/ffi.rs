// SPDX-License-Identifier: Apache-2.0 OR MIT

/*
 * Copyright © 2024 The lnix Authors
 *
 * Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
 * https://www.apache.org/licenses/LICENSE-2.0> or the MIT license
 * <LICENSE-MIT or https://opensource.org/licenses/MIT>, at your
 * option. This file may not be copied, modified, or distributed
 * except according to those terms.
 */

//! Supplements for `std::ffi`.

use std::ffi::{OsStr, OsString};
use std::os::unix::prelude::OsStrExt as _;

pub mod ctypes {
    #![allow(missing_docs)]
    #![allow(non_camel_case_types)]

    pub use core::ffi::{
        c_char, c_double, c_float, c_int, c_long, c_longlong, c_schar, c_short, c_uchar, c_uint,
        c_ulong, c_ulonglong, c_ushort, c_void,
    };

    pub type c_size_t = usize;
    pub type c_ssize_t = isize;
}

/// Extension trait for [`OsStr`].
pub trait OsStrExt {
    /// Returns `true` if this `OsStr` starts with `prefix`.
    fn starts_with<S: AsRef<OsStr>>(&self, prefix: S) -> bool;
    /// Returns `true` if this `OsStr` contains `x`.
    fn contains(&self, x: u8) -> bool;
}
impl OsStrExt for OsStr {
    fn starts_with<S: AsRef<OsStr>>(&self, prefix: S) -> bool {
        self.as_bytes().starts_with(prefix.as_ref().as_bytes())
    }

    fn contains(&self, x: u8) -> bool {
        self.as_bytes().contains(&x)
    }
}

/// Extension trait for [`OsString`].
pub trait OsStringExt {
    /// Returns `true` if this `OsString` starts with `prefix`.
    fn starts_with<S: AsRef<OsStr>>(&self, prefix: S) -> bool;
    /// Returns `true` if this `OsString` contains `x`.
    fn contains(&self, x: u8) -> bool;
}
impl OsStringExt for OsString {
    fn starts_with<S: AsRef<OsStr>>(&self, prefix: S) -> bool {
        self.as_bytes().starts_with(prefix.as_ref().as_bytes())
    }

    fn contains(&self, x: u8) -> bool {
        self.as_bytes().contains(&x)
    }
}
