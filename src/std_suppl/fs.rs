// SPDX-License-Identifier: Apache-2.0 OR MIT

/*
 * Copyright © 2024 The lnix Authors
 *
 * Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
 * https://www.apache.org/licenses/LICENSE-2.0> or the MIT license
 * <LICENSE-MIT or https://opensource.org/licenses/MIT>, at your
 * option. This file may not be copied, modified, or distributed
 * except according to those terms.
 */

//! Supplements for `std::fs`.

use std::fs::File;
use std::io::Result as IoResult;
use std::os::unix::prelude::*;
use std::path::Path;

use rustix::fs::{Mode, OFlags, openat};

/// Extension trait for [`File`].
pub trait FileExt: Sized {
    /// Attempts to open a file in read-only mode.
    fn open_at<FD: AsFd, P: AsRef<Path>>(dirfd: FD, path: P) -> IoResult<Self>;
    /// Opens a file in write-only mode.
    fn create_at<FD: AsFd, P: AsRef<Path>>(dirfd: FD, path: P) -> IoResult<Self>;
    /// Creates a new file in read-write mode; error if the file exists.
    fn create_new_at<FD: AsFd, P: AsRef<Path>>(dirfd: FD, path: P) -> IoResult<Self>;
}
impl FileExt for File {
    fn open_at<FD: AsFd, P: AsRef<Path>>(dirfd: FD, path: P) -> IoResult<Self> {
        let fd = openat(
            dirfd.as_fd(),
            path.as_ref(),
            OFlags::CLOEXEC | OFlags::RDONLY,
            Mode::empty(),
        )?;

        Ok(File::from(fd))
    }

    fn create_at<FD: AsFd, P: AsRef<Path>>(dirfd: FD, path: P) -> IoResult<Self> {
        let fd = openat(
            dirfd.as_fd(),
            path.as_ref(),
            OFlags::CLOEXEC | OFlags::WRONLY | OFlags::CREATE | OFlags::TRUNC,
            Mode::from_raw_mode(0o666),
        )?;

        Ok(File::from(fd))
    }

    fn create_new_at<FD: AsFd, P: AsRef<Path>>(dirfd: FD, path: P) -> IoResult<Self> {
        let fd = openat(
            dirfd.as_fd(),
            path.as_ref(),
            OFlags::CLOEXEC | OFlags::RDWR | OFlags::CREATE | OFlags::EXCL,
            Mode::from_raw_mode(0o666),
        )?;

        Ok(File::from(fd))
    }
}
