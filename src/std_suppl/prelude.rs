// SPDX-License-Identifier: Apache-2.0 OR MIT

/*
 * Copyright © 2022 The lnix Authors
 *
 * Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
 * https://www.apache.org/licenses/LICENSE-2.0> or the MIT license
 * <LICENSE-MIT or https://opensource.org/licenses/MIT>, at your
 * option. This file may not be copied, modified, or distributed
 * except according to those terms.
 */

//! Prelude for `std_suppl`.

pub use super::fd::FdExt;
pub use super::ffi::OsStrExt;
pub use super::fs::FileExt;
pub use super::path::{PathBufExt, PathExt};
