// SPDX-License-Identifier: Apache-2.0 OR MIT

/*
 * Copyright © 2022 The lnix Authors
 *
 * Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
 * https://www.apache.org/licenses/LICENSE-2.0> or the MIT license
 * <LICENSE-MIT or https://opensource.org/licenses/MIT>, at your
 * option. This file may not be copied, modified, or distributed
 * except according to those terms.
 */

//! Supplements for `std::io`.

use std::io::ErrorKind;

/// Extension trait for `ErrorKind`.
pub trait ErrorKindExt {
    #![expect(clippy::wrong_self_convention, reason = "std::io::ErrorKind is Copy")]
    /// Returns `true` if the `ErrorKind` is `NotFound`.
    fn is_not_found(self) -> bool;
    /// Returns `true` if the `ErrorKind` is `PermissionDenied`.
    fn is_permission_denied(self) -> bool;
    /// Returns `true` if the `ErrorKind` is `ConnectionRefused`.
    fn is_connection_refused(self) -> bool;
    /// Returns `true` if the `ErrorKind` is `ConnectionReset`.
    fn is_connection_reset(self) -> bool;
    /// Returns `true` if the `ErrorKind` is `HostUnreachable`.
    fn is_host_unreachable(self) -> bool;
    /// Returns `true` if the `ErrorKind` is `NetworkUnreachable`.
    fn is_network_unreachable(self) -> bool;
    /// Returns `true` if the `ErrorKind` is `ConnectionAborted`.
    fn is_connection_aborted(self) -> bool;
    /// Returns `true` if the `ErrorKind` is `NotConnected`.
    fn is_not_connected(self) -> bool;
    /// Returns `true` if the `ErrorKind` is `AddrInUse`.
    fn is_addr_in_use(self) -> bool;
    /// Returns `true` if the `ErrorKind` is `AddrNotAvailable`.
    fn is_addr_not_available(self) -> bool;
    /// Returns `true` if the `ErrorKind` is `NetworkDown`.
    fn is_network_down(self) -> bool;
    /// Returns `true` if the `ErrorKind` is `BrokenPipe`.
    fn is_broken_pipe(self) -> bool;
    /// Returns `true` if the `ErrorKind` is `AlreadyExists`.
    fn is_already_exists(self) -> bool;
    /// Returns `true` if the `ErrorKind` is `WouldBlock`.
    fn is_would_block(self) -> bool;
    /// Returns `true` if the `ErrorKind` is `NotADirectory`.
    fn is_not_a_directory(self) -> bool;
    /// Returns `true` if the `ErrorKind` is `IsADirectory`.
    fn is_is_a_directory(self) -> bool;
    /// Returns `true` if the `ErrorKind` is `DirectoryNotEmpty`.
    fn is_directory_not_empty(self) -> bool;
    /// Returns `true` if the `ErrorKind` is `ReadOnlyFilesystem`.
    fn is_read_only_filesystem(self) -> bool;
    // unstable: fn is_filesystem_loop(self) -> bool;
    /// Returns `true` if the `ErrorKind` is `StaleNetworkFileHandle`.
    fn is_stale_network_file_handle(self) -> bool;
    /// Returns `true` if the `ErrorKind` is `InvalidInput`.
    fn is_invalid_input(self) -> bool;
    /// Returns `true` if the `ErrorKind` is `InvalidData`.
    fn is_invalid_data(self) -> bool;
    /// Returns `true` if the `ErrorKind` is `TimedOut`.
    fn is_timed_out(self) -> bool;
    /// Returns `true` if the `ErrorKind` is `WriteZero`.
    fn is_write_zero(self) -> bool;
    /// Returns `true` if the `ErrorKind` is `StorageFull`.
    fn is_storage_full(self) -> bool;
    /// Returns `true` if the `ErrorKind` is `NotSeekable`.
    fn is_not_seekable(self) -> bool;
    /// Returns `true` if the `ErrorKind` is `QuotaExceeded`.
    fn is_quota_exceeded(self) -> bool;
    /// Returns `true` if the `ErrorKind` is `FileTooLarge`.
    fn is_file_too_large(self) -> bool;
    /// Returns `true` if the `ErrorKind` is `ResourceBusy`.
    fn is_resource_busy(self) -> bool;
    /// Returns `true` if the `ErrorKind` is `ExecutableFileBusy`.
    fn is_executable_file_busy(self) -> bool;
    /// Returns `true` if the `ErrorKind` is `Deadlock`.
    fn is_deadlock(self) -> bool;
    /// Returns `true` if the `ErrorKind` is `CrossesDevices`.
    fn is_crosses_devices(self) -> bool;
    /// Returns `true` if the `ErrorKind` is `TooManyLinks`.
    fn is_too_many_links(self) -> bool;
    // unstable: fn is_invalid_filename(self) -> bool;
    /// Returns `true` if the `ErrorKind` is `ArgumentListTooLong`.
    fn is_argument_list_too_long(self) -> bool;
    /// Returns `true` if the `ErrorKind` is `Interrupted`.
    fn is_interrupted(self) -> bool;
    /// Returns `true` if the `ErrorKind` is `Unsupported`.
    fn is_unsupported(self) -> bool;
    /// Returns `true` if the `ErrorKind` is `UnexpectedEof`.
    fn is_unexpected_eof(self) -> bool;
    /// Returns `true` if the `ErrorKind` is `OutOfMemory`.
    fn is_out_of_memory(self) -> bool;
    // unstable: fn is_in_progress(self) -> bool;
    /// Returns `true` if the `ErrorKind` is `Other`.
    fn is_other(self) -> bool;
}

impl ErrorKindExt for ErrorKind {
    fn is_not_found(self) -> bool {
        matches!(self, ErrorKind::NotFound)
    }

    fn is_permission_denied(self) -> bool {
        matches!(self, ErrorKind::PermissionDenied)
    }

    fn is_connection_refused(self) -> bool {
        matches!(self, ErrorKind::ConnectionRefused)
    }

    fn is_connection_reset(self) -> bool {
        matches!(self, ErrorKind::ConnectionReset)
    }

    fn is_host_unreachable(self) -> bool {
        matches!(self, ErrorKind::HostUnreachable)
    }

    fn is_network_unreachable(self) -> bool {
        matches!(self, ErrorKind::NetworkUnreachable)
    }

    fn is_connection_aborted(self) -> bool {
        matches!(self, ErrorKind::ConnectionAborted)
    }

    fn is_not_connected(self) -> bool {
        matches!(self, ErrorKind::NotConnected)
    }

    fn is_addr_in_use(self) -> bool {
        matches!(self, ErrorKind::AddrInUse)
    }

    fn is_addr_not_available(self) -> bool {
        matches!(self, ErrorKind::AddrNotAvailable)
    }

    fn is_network_down(self) -> bool {
        matches!(self, ErrorKind::NetworkDown)
    }

    fn is_broken_pipe(self) -> bool {
        matches!(self, ErrorKind::BrokenPipe)
    }

    fn is_already_exists(self) -> bool {
        matches!(self, ErrorKind::AlreadyExists)
    }

    fn is_would_block(self) -> bool {
        matches!(self, ErrorKind::WouldBlock)
    }

    fn is_not_a_directory(self) -> bool {
        matches!(self, ErrorKind::NotADirectory)
    }

    fn is_is_a_directory(self) -> bool {
        matches!(self, ErrorKind::IsADirectory)
    }

    fn is_directory_not_empty(self) -> bool {
        matches!(self, ErrorKind::DirectoryNotEmpty)
    }

    fn is_read_only_filesystem(self) -> bool {
        matches!(self, ErrorKind::ReadOnlyFilesystem)
    }

    fn is_stale_network_file_handle(self) -> bool {
        matches!(self, ErrorKind::StaleNetworkFileHandle)
    }

    fn is_invalid_input(self) -> bool {
        matches!(self, ErrorKind::InvalidInput)
    }

    fn is_invalid_data(self) -> bool {
        matches!(self, ErrorKind::InvalidData)
    }

    fn is_timed_out(self) -> bool {
        matches!(self, ErrorKind::TimedOut)
    }

    fn is_write_zero(self) -> bool {
        matches!(self, ErrorKind::WriteZero)
    }

    fn is_storage_full(self) -> bool {
        matches!(self, ErrorKind::StorageFull)
    }

    fn is_not_seekable(self) -> bool {
        matches!(self, ErrorKind::NotSeekable)
    }

    fn is_quota_exceeded(self) -> bool {
        matches!(self, ErrorKind::QuotaExceeded)
    }

    fn is_file_too_large(self) -> bool {
        matches!(self, ErrorKind::FileTooLarge)
    }

    fn is_resource_busy(self) -> bool {
        matches!(self, ErrorKind::ResourceBusy)
    }

    fn is_executable_file_busy(self) -> bool {
        matches!(self, ErrorKind::ExecutableFileBusy)
    }

    fn is_deadlock(self) -> bool {
        matches!(self, ErrorKind::Deadlock)
    }

    fn is_crosses_devices(self) -> bool {
        matches!(self, ErrorKind::CrossesDevices)
    }

    fn is_too_many_links(self) -> bool {
        matches!(self, ErrorKind::TooManyLinks)
    }

    fn is_argument_list_too_long(self) -> bool {
        matches!(self, ErrorKind::ArgumentListTooLong)
    }

    fn is_interrupted(self) -> bool {
        matches!(self, ErrorKind::Interrupted)
    }

    fn is_unsupported(self) -> bool {
        matches!(self, ErrorKind::Unsupported)
    }

    fn is_unexpected_eof(self) -> bool {
        matches!(self, ErrorKind::UnexpectedEof)
    }

    fn is_out_of_memory(self) -> bool {
        matches!(self, ErrorKind::OutOfMemory)
    }

    fn is_other(self) -> bool {
        matches!(self, ErrorKind::Other)
    }
}
