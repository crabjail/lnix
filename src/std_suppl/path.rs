// SPDX-License-Identifier: Apache-2.0 OR MIT

/*
 * Copyright © 2022 The lnix Authors
 *
 * Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
 * https://www.apache.org/licenses/LICENSE-2.0> or the MIT license
 * <LICENSE-MIT or https://opensource.org/licenses/MIT>, at your
 * option. This file may not be copied, modified, or distributed
 * except according to those terms.
 */

//! Supplements for `std::path`.

use std::ffi::OsString;
use std::io::Result as IoResult;
use std::os::unix::prelude::*;
use std::path::{Path, PathBuf};

/// Extension trait for [`Path`].
pub trait PathExt {
    /// Gets the underlying byte view of the `Path`.
    fn as_bytes(&self) -> &[u8];
    /// Returns the length of the underlying `OsStr`.
    fn len(&self) -> usize;
    /// Returns `true` if the path has a length of 0.
    fn is_empty(&self) -> bool;

    /// Returns the canonical, absolute form of the path with all intermediate components normalized and symbolic links resolved except for the final component.
    fn canonicalize_nofollow(&self) -> IoResult<PathBuf>;
    /// Returns `true` if the path is the root directory.
    fn is_root(&self) -> bool;
    /// Strips the root prefix from this path, making it a relative path to the root directory.
    ///
    /// Passing a relative path either returns the path as is or panics.
    fn strip_root(&self) -> &Path;
}
impl PathExt for Path {
    fn as_bytes(&self) -> &[u8] {
        self.as_os_str().as_bytes()
    }

    fn len(&self) -> usize {
        self.as_os_str().len()
    }

    fn is_empty(&self) -> bool {
        self.as_os_str().is_empty()
    }

    fn canonicalize_nofollow(&self) -> IoResult<PathBuf> {
        Ok(if let Some(file_name) = self.file_name() {
            let canonicalized_path = self.parent().unwrap().canonicalize()?.join(file_name);
            canonicalized_path.symlink_metadata()?;
            canonicalized_path
        } else {
            self.canonicalize()?
        })
    }

    fn is_root(&self) -> bool {
        self == Path::new("/")
    }

    fn strip_root(&self) -> &Path {
        self.strip_prefix("/")
            .expect("strip_root called on a relative path")
    }
}

/// Extension trait for [`PathBuf`].
pub trait PathBufExt {
    /// Converts the `PathBuf` into a `String` if it contains valid Unicode data.
    ///
    /// On failure, ownership of the underlying `OsString` is returned.
    fn into_string(self) -> Result<String, OsString>;
    /// Yields the underlying byte vector of this `PathBuf`.
    fn into_vec(self) -> Vec<u8>;
}
impl PathBufExt for PathBuf {
    fn into_string(self) -> Result<String, OsString> {
        self.into_os_string().into_string()
    }

    fn into_vec(self) -> Vec<u8> {
        self.into_os_string().into_vec()
    }
}
