// SPDX-License-Identifier: Apache-2.0 OR MIT

/*
 * Copyright © 2024 The lnix Authors
 *
 * Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
 * https://www.apache.org/licenses/LICENSE-2.0> or the MIT license
 * <LICENSE-MIT or https://opensource.org/licenses/MIT>, at your
 * option. This file may not be copied, modified, or distributed
 * except according to those terms.
 */

//! `execve`

use core::ffi::*;
use std::borrow::Cow;
use std::ffi::OsStr;
use std::os::unix::prelude::*;
use std::path::PathBuf;
use std::{env, ptr};

use libc::AT_SYMLINK_NOFOLLOW;
use linux_raw_sys::general::{AT_EMPTY_PATH, AT_FDCWD};
use rustix::path::Arg;

use crate::error::{Errno, cvt};

#[derive(Debug)]
struct Argv<'a> {
    #[expect(dead_code, reason = "Used via raw pointers")]
    c_str: Vec<Cow<'a, CStr>>,
    ptrs: Vec<*mut c_char>,
}
impl<'a> Argv<'a> {
    fn new<A: IntoIterator<Item: Arg + 'a>>(argv: A) -> Result<Self, Errno> {
        let c_str: Vec<Cow<'a, CStr>> = argv
            .into_iter()
            .map(|a| a.into_c_str())
            .collect::<Result<_, _>>()?;
        let mut ptrs: Vec<*mut c_char> = c_str.iter().map(|a| a.as_ptr().cast_mut()).collect();
        ptrs.push(ptr::null_mut());

        Ok(Self { c_str, ptrs })
    }
}
impl Argv<'_> {
    fn as_ptr(&self) -> *const *mut c_char {
        self.ptrs.as_ptr()
    }
}

#[derive(Debug)]
enum Envp<'e> {
    Inherit,
    NewEnv {
        #[expect(dead_code, reason = "Used via raw pointers")]
        c_str: Vec<Cow<'e, CStr>>,
        ptrs: Vec<*mut c_char>,
    },
}
impl<'e> Envp<'e> {
    fn new<A: IntoIterator<Item: Arg + 'e>>(envp: A) -> Result<Self, Errno> {
        let c_str: Vec<Cow<'e, CStr>> = envp
            .into_iter()
            .map(|a| a.into_c_str())
            .collect::<Result<_, _>>()?;
        let mut ptrs: Vec<*mut c_char> = c_str.iter().map(|a| a.as_ptr().cast_mut()).collect();
        ptrs.push(ptr::null_mut());

        Ok(Self::NewEnv { c_str, ptrs })
    }
}
impl Envp<'_> {
    fn as_ptr(&self) -> *const *mut c_char {
        match self {
            Self::Inherit => unsafe { environ },
            Self::NewEnv { ptrs, .. } => ptrs.as_ptr(),
        }
    }
}

/// A `Program` that can be `execve`d.
#[derive(Debug)]
pub struct Program<'p, 'a, 'e> {
    dirfd: Option<OwnedFd>,
    pathname: Cow<'p, CStr>,
    argv: Option<Argv<'a>>,
    envp: Option<Envp<'e>>,

    at_empty_path: bool,
    at_symlink_nofollow: bool,

    path: Vec<PathBuf>,
    search_path: Option<Vec<u8>>,
}
impl<'p, 'a, 'e> Program<'p, 'a, 'e> {
    /// Creates a new `Program`.
    pub fn new<P>(pathname: P) -> Result<Self, Errno>
    where
        P: Arg + 'p,
    {
        Ok(Self {
            dirfd: None,
            pathname: pathname.into_c_str()?,
            argv: None,
            envp: None,

            at_empty_path: false,
            at_symlink_nofollow: false,

            path: Vec::new(),
            search_path: None,
        })
    }

    /// Creates a new `Program`.
    pub fn with_dirfd<P>(dirfd: OwnedFd, pathname: P) -> Result<Self, Errno>
    where
        P: Arg + 'p,
    {
        Ok(Self {
            dirfd: Some(dirfd),
            pathname: pathname.into_c_str()?,
            argv: None,
            envp: None,

            at_empty_path: false,
            at_symlink_nofollow: false,

            path: Vec::new(),
            search_path: None,
        })
    }

    /// Pass arguments.
    pub fn argv<A>(&mut self, argv: A) -> Result<&mut Self, Errno>
    where
        A: IntoIterator<Item: Arg + 'a>,
    {
        self.argv = Some(Argv::new(argv)?);
        Ok(self)
    }

    /// Create a new environment.
    pub fn envp<A>(&mut self, envp: A) -> Result<&mut Self, Errno>
    where
        A: IntoIterator<Item: Arg + 'e>,
    {
        self.envp = Some(Envp::new(envp)?);
        Ok(self)
    }

    /// Inherit the environment.
    pub fn envp_inherit(&mut self) -> &mut Self {
        self.envp = Some(Envp::Inherit);
        self
    }

    /// Add the `AT_EMPTY_PATH` flag.
    pub fn at_empty_path(&mut self) -> &mut Self {
        self.at_empty_path = true;
        self
    }

    /// Add the `AT_SYMLINK_NOFOLLOW` flag.
    pub fn at_symlink_nofollow(&mut self) -> &mut Self {
        self.at_symlink_nofollow = true;
        self
    }

    /// Search in `$PATH` for pathname.
    ///
    /// If `dirfd` is used or `pathname` contains a `/` no path search is performed.
    pub fn search_path(&mut self) -> &mut Self {
        self.path = env::split_paths(
            &env::var_os("PATH").map_or(Cow::Borrowed(OsStr::new("/usr/bin")), Cow::Owned),
        )
        .collect();
        self.search_path = Some(Vec::with_capacity(4096));
        self
    }

    /// Replaces the program in the current process.
    #[doc(alias = "execve")]
    #[doc(alias = "execveat")]
    pub fn exec(&mut self) -> Errno {
        if self.search_path.is_none()
            || self.dirfd.is_some()
            || self.pathname.to_bytes().contains(&b'/')
        {
            self.actual_exec(&self.pathname)
        } else if let Some(mut buf) = self.search_path.take() {
            for path in &self.path {
                let length = path.as_os_str().len() + 1 + self.pathname.count_bytes() + 1;
                if length > buf.capacity() {
                    continue;
                }
                buf.clear();
                buf.extend_from_slice(path.as_os_str().as_bytes());
                buf.push(b'/');
                buf.extend_from_slice(self.pathname.to_bytes_with_nul());
                let Ok(cstr) = CStr::from_bytes_with_nul(&buf[..length]) else {
                    continue;
                };
                let errno = self.actual_exec(cstr);
                if errno == Errno::ENOENT {
                    continue;
                }
                return errno;
            }
            Errno::ENOENT
        } else {
            unsafe { release_unreachable_unchecked!() }
        }
    }

    fn actual_exec(&self, pathname: &CStr) -> Errno {
        let mut flags: i32 = 0;
        if self.at_empty_path {
            flags |= AT_EMPTY_PATH as i32;
        }
        if self.at_symlink_nofollow {
            flags |= AT_SYMLINK_NOFOLLOW;
        }

        cvt(unsafe {
            execveat(
                self.dirfd.as_ref().map_or(AT_FDCWD, |fd| fd.as_raw_fd()),
                pathname.as_ptr(),
                self.argv.as_ref().map_or(ptr::null(), |argv| argv.as_ptr()),
                self.envp.as_ref().map_or(ptr::null(), |envp| envp.as_ptr()),
                flags,
            )
        })
        .unwrap_err()
    }
}

unsafe extern "C" {
    static mut environ: *mut *mut c_char;

    // fn execve(prog: *const c_char, argv: *const *mut c_char, envp: *const *mut c_char) -> c_int;

    fn execveat(
        dirfd: c_int,
        pathname: *const c_char,
        argv: *const *mut c_char,
        envp: *const *mut c_char,
        flags: c_int,
    ) -> c_int;
}

#[cfg(target_env = "musl")]
mod musl_polyfill {
    use core::ffi::*;

    #[unsafe(no_mangle)]
    unsafe extern "C" fn execveat(
        dirfd: c_int,
        pathname: *const c_char,
        argv: *const *mut c_char,
        envp: *const *mut c_char,
        flags: c_int,
    ) -> c_int {
        unsafe { libc::syscall(libc::SYS_execveat, dirfd, pathname, argv, envp, flags) as c_int }
    }
}
