// SPDX-License-Identifier: Apache-2.0 OR MIT

/*
 * Copyright © 2024 The lnix Authors
 *
 * Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
 * https://www.apache.org/licenses/LICENSE-2.0> or the MIT license
 * <LICENSE-MIT or https://opensource.org/licenses/MIT>, at your
 * option. This file may not be copied, modified, or distributed
 * except according to those terms.
 */

//! `clone`.

use crate::error::{Errno, cvt};

use super::Pid;

// #[derive(Debug)]
// pub struct ProcessBuilder {}
// impl ProcessBuilder {
//     pub fn clone<F>(&self, func: F) -> Result<(), Errno>
//     where
//         F: FnOnce() -> ExitCode,
//     {
//         unimplemented!()
//     }
// }

/// Creates a new child process.
///
/// # Safety
///
/// `fork` is unsafe for multiple reasons.
pub unsafe fn fork() -> Result<Option<Pid>, Errno> {
    match cvt(unsafe { libc::fork() })? {
        0 => Ok(None),
        pid => Ok(Some(Pid::from_raw(pid).unwrap())),
    }
}
