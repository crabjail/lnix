// SPDX-License-Identifier: Apache-2.0 OR MIT

/*
 * Copyright © 2023 The lnix Authors
 *
 * Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
 * https://www.apache.org/licenses/LICENSE-2.0> or the MIT license
 * <LICENSE-MIT or https://opensource.org/licenses/MIT>, at your
 * option. This file may not be copied, modified, or distributed
 * except according to those terms.
 */

//! Process operations.

use std::os::unix::prelude::*;

use bitflags::bitflags;

use crate::error::{Errno, cvt};

mod clone;
mod credentials;
mod execve;

pub use self::clone::fork;
pub use self::credentials::{Pid, setegid, seteuid, setgid, setuid};
pub use self::execve::Program;
pub use crate::ugid::{Gid, RawGid, RawUid, Uid};
pub use rustix::process::RawPid;

bitflags! {
    /// Possible namespaces for [`setns`] and others.
    pub struct NsType: i32 {
        /// Allow any namespace type to be joined.
        const ANY = 0;
        /// `cgroup_namespaces(7)`
        const CGROUP = libc::CLONE_NEWCGROUP;
        /// `ipc_namespaces(7)`
        const IPC = libc::CLONE_NEWIPC;
        /// `network_namespaces(7)`
        const NET = libc::CLONE_NEWNET;
        /// `mount_namespaces(7)`
        const MNT = libc::CLONE_NEWNS;
        /// `pid_namespaces(7)`
        const PID = libc::CLONE_NEWPID;
        /// `time_namespaces(7)`
        const TIME = linux_raw_sys::general::CLONE_NEWTIME as i32;
        /// `user_namespaces(7)`
        const USER = libc::CLONE_NEWUSER;
        /// `uts_namespaces(7)`
        const UTS = libc::CLONE_NEWUTS;
    }
}

/// Moves the calling thread into a different namespace.
///
/// - [`rustix::thread::move_into_link_name_space`]
/// - [`rustix::thread::move_into_thread_name_spaces`]
pub fn setns<FD: AsFd>(fd: FD, nstype: NsType) -> Result<(), Errno> {
    cvt(unsafe { libc::setns(fd.as_fd().as_raw_fd(), nstype.bits()) })?;

    Ok(())
}
