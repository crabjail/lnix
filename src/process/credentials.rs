// SPDX-License-Identifier: Apache-2.0 OR MIT

/*
 * Copyright © 2023 The lnix Authors
 *
 * Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
 * https://www.apache.org/licenses/LICENSE-2.0> or the MIT license
 * <LICENSE-MIT or https://opensource.org/licenses/MIT>, at your
 * option. This file may not be copied, modified, or distributed
 * except according to those terms.
 */

//! Process operations.

use rustix::process::RawPid;

use crate::error::{Errno, cvt};
use crate::ugid::{Gid, Uid};

impl Uid {
    /// Gets the real uid of the calling process.
    #[doc(alias = "getuid")]
    pub fn real() -> Self {
        rustix::process::getuid().into()
    }

    /// Gets the effective uid of the calling process.
    #[doc(alias = "geteuid")]
    pub fn effective() -> Self {
        rustix::process::geteuid().into()
    }
}
impl From<rustix::process::Uid> for Uid {
    fn from(uid: rustix::process::Uid) -> Self {
        Self(uid.as_raw())
    }
}
impl From<Uid> for rustix::process::Uid {
    fn from(uid: Uid) -> Self {
        debug_assert!(uid.0 != -1_i32 as u32);
        unsafe { Self::from_raw(uid.0) }
    }
}

impl Gid {
    /// Gets the real gid of the calling process.
    #[doc(alias = "getgid")]
    pub fn real() -> Self {
        rustix::process::getgid().into()
    }

    /// Gets the effective gid of the calling process.
    #[doc(alias = "getegid")]
    pub fn effective() -> Self {
        rustix::process::getegid().into()
    }
}
impl From<rustix::process::Gid> for Gid {
    fn from(gid: rustix::process::Gid) -> Self {
        Self(gid.as_raw())
    }
}
impl From<Gid> for rustix::process::Gid {
    fn from(gid: Gid) -> Self {
        debug_assert!(gid.0 != -1_i32 as u32);
        unsafe { Self::from_raw(gid.0) }
    }
}

/// Sets the uid of the calling process.
pub fn setuid(uid: Uid) -> Result<(), Errno> {
    cvt(unsafe { libc::setuid(uid.as_raw()) })?;

    Ok(())
}

/// Sets the effective uid of the calling process.
pub fn seteuid(uid: Uid) -> Result<(), Errno> {
    cvt(unsafe { libc::seteuid(uid.as_raw()) })?;

    Ok(())
}

/// Sets the gid of the calling process.
pub fn setgid(gid: Gid) -> Result<(), Errno> {
    cvt(unsafe { libc::setgid(gid.as_raw()) })?;

    Ok(())
}

/// Sets the effective gid of the calling process.
pub fn setegid(gid: Gid) -> Result<(), Errno> {
    cvt(unsafe { libc::setegid(gid.as_raw()) })?;

    Ok(())
}

/// A pid.
#[derive(Debug, Default, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[repr(transparent)]
pub struct Pid(RawPid);
impl Pid {
    /// Gets the pid of the calling process.
    #[doc(alias = "getpid")]
    pub fn current() -> Self {
        rustix::process::getpid().into()
    }

    /// Gets the pid of the parent of the calling process.
    #[doc(alias = "getppid")]
    pub fn parent() -> Option<Self> {
        Some(rustix::process::getppid()?.into())
    }

    /// Converts a `Pid` to a `RawPid`.
    pub fn as_raw(self) -> RawPid {
        self.0
    }

    /// Converts a `RawPid` to a `Pid`.
    pub fn from_raw(raw_pid: RawPid) -> Option<Self> {
        if raw_pid > 0 {
            Some(Self(raw_pid))
        } else {
            None
        }
    }
}
impl From<rustix::process::Pid> for Pid {
    fn from(pid: rustix::process::Pid) -> Self {
        Self(rustix::process::Pid::as_raw(Some(pid)))
    }
}
impl From<Pid> for rustix::process::Pid {
    fn from(pid: Pid) -> Self {
        debug_assert!(pid.0 > 0);
        unsafe { Self::from_raw_unchecked(pid.0) }
    }
}
