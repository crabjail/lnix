// SPDX-License-Identifier: Apache-2.0 OR MIT

/*
 * Copyright © 2023 The lnix Authors
 *
 * Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
 * https://www.apache.org/licenses/LICENSE-2.0> or the MIT license
 * <LICENSE-MIT or https://opensource.org/licenses/MIT>, at your
 * option. This file may not be copied, modified, or distributed
 * except according to those terms.
 */

use std::mem;
use std::os::unix::prelude::*;

use rustix::event::{EventfdFlags, eventfd};

use crate::error::Errno;

/// eventfd
#[derive(Debug)]
pub struct EventFd(OwnedFd);
impl EventFd {
    /// Creates a new `EventFd`.
    pub fn new(initval: u32) -> Result<Self, Errno> {
        let fd = eventfd(initval, EventfdFlags::CLOEXEC)?;
        Ok(Self(fd))
    }

    /// Reads the value from this `EventFd`.
    ///
    /// # Panics
    ///
    /// This function panics on partial reads.
    pub fn read(&self) -> Result<u64, Errno> {
        let mut buf = 0_u64.to_ne_bytes();
        let bytes_read = rustix::io::read(&self.0, &mut buf)?;
        assert_eq!(bytes_read, mem::size_of::<u64>());

        let counter = u64::from_ne_bytes(buf);

        Ok(counter)
    }

    /// Writes `value` to this `EventFd`.
    ///
    /// # Panics
    ///
    /// This function panics on partial writes.
    pub fn write(&self, value: u64) -> Result<(), Errno> {
        let bytes_written = rustix::io::write(&self.0, &value.to_ne_bytes())?;
        assert_eq!(bytes_written, mem::size_of::<u64>());

        Ok(())
    }

    // TODO: poll
}
impl AsFd for EventFd {
    fn as_fd(&self) -> BorrowedFd<'_> {
        self.0.as_fd()
    }
}
impl AsRawFd for EventFd {
    fn as_raw_fd(&self) -> RawFd {
        self.0.as_raw_fd()
    }
}
impl FromRawFd for EventFd {
    unsafe fn from_raw_fd(fd: RawFd) -> Self {
        Self(unsafe { OwnedFd::from_raw_fd(fd) })
    }
}
impl IntoRawFd for EventFd {
    fn into_raw_fd(self) -> RawFd {
        self.0.into_raw_fd()
    }
}
